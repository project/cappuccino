<?php

/**
 * @file
 * Enables modules and site configuration for a cappuccino site installation.
 */

use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function cappuccino_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  $form['site_information']['#access'] = TRUE;
  $form['site_information']['site_name']['#access'] = TRUE;
  $form['site_information']['site_mail']['#access'] = TRUE;

  $form['#submit'][] = 'cappuccino_form_install_configure_submit';
}


/**
* Submission handler to sync the contact.form.feedback recipient.
*/
function cappuccino_form_install_configure_submit($form, FormStateInterface $form_state) {
  $site_mail = $form_state->getValue('site_mail');
  ContactForm::load('feedback')->setRecipients([$site_mail])->trustData()->save();

  $config = \Drupal::configFactory()->getEditable('system.site');
  $config
    ->set('name', $form_state->getValue('site_name'))
    ->set('mail', $form_state->getValue('site_mail'))
    ->save();
}
