/**
 * @file
 * Global utilities.
 *
 */
(function (Drupal) {

  'use strict';

  Drupal.behaviors.megamenu = {
    attach: function (context, settings) {

      // Make submenu full browser width.
      const submenuFullwidthCalc = function () {
        // Get the Mega menu Level 1 sub menu.
        document.querySelectorAll(".tbm-nav > .level-1 > .tbm-submenu").forEach(function (submenu) {
          submenu.style.left = '0px';

          const offsetTarget = document.body.getBoundingClientRect();
          // The offset of this submenu's parent ul.
          const offsetThis = submenu.closest("ul").getBoundingClientRect();
          // Calculate the offset.
          submenu.style.left = `${offsetTarget.left - offsetThis.left}px`;

          // Set the submenu full width.
          submenu.style.width = `${document.body.offsetWidth}px`;
        });
      };

      window.addEventListener('load', submenuFullwidthCalc);
      window.addEventListener('resize', submenuFullwidthCalc);
    }

  };

})(Drupal);
