/**
 * @file
 * Global utilities.
 *
 */
(function (Drupal) {

  'use strict';

  Drupal.behaviors.stickyHeader = {
    attach: function (context, settings) {
      window.addEventListener('scroll', function() {
        if (window.scrollY >= 100) {
          document.getElementById('header').classList.add('scrolled');
          document.body.classList.add('scrolled');
        } else {
          document.getElementById('header').classList.remove('scrolled');
          document.body.classList.remove('scrolled');
        }
      });
    }
  };

})(Drupal);
