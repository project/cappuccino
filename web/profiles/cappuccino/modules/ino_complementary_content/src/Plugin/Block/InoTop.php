<?php

namespace Drupal\ino_complementary_content\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\ino_complementary_content\InoCustomBlock;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\site_settings\Entity\SiteSettingEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class InoTop.
 *
 * @package Drupal\ino_complementary_content\Plugin\Block
 */
#[Block(id: 'top_block', admin_label: new TranslatableMarkup('Custom top content'), category: new TranslatableMarkup('Custom blocks'))]
class InoTop extends InoCustomBlock {

  /**
   * The current request object.
   *
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * {@inheritdoc}
   */
  protected $siteSettingsType = 'ino_ss_top';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager, RequestStack $requestStack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $languageManager);
    $this->currentRequest = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function build() {
    $build = [];
    /** @var \Drupal\site_settings\Entity\SiteSettingEntity $header_settings */
    $top_settings = $this->loadSiteSettings();

    $output = $this->getParagraphs($top_settings);
    if (empty($output)) {
      return $build;
    }

    $build = [
      '#theme' => 'ino_top',
      '#paragraphs' => $output,
    ];

    return $build;
  }

  /**
   * Loads the paragraphs from the configuration page.
   *
   * @param null|\Drupal\site_settings\Entity\SiteSettingEntity $settings
   *   The source site_settings entity.
   *
   * @return array
   *   The paragraphs render array.
   */
  protected function getParagraphs($settings) {
    $paragraphs = [];

    if (NULL !== $settings) {
      $config_paragraphs = $this->getSettingParagraphs($settings);
      if (is_array($config_paragraphs)) {
        $paragraphs = array_merge($paragraphs, $config_paragraphs);
      }
    }

    $currentNode = $this->currentRequest->get('node');
    if ($currentNode instanceof Node && $currentNode->hasField('field_n_top')) {
      $paragraphs = array_merge($paragraphs, $this->getNodeParagraphs($currentNode));
    }

    $view_builder = $this->entityTypeManager->getViewBuilder('paragraph');

    return $view_builder->viewMultiple($paragraphs ?: []);
  }

  /**
   * Loads the paragraphs from the current node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @return array
   *   The paragraphs render array.
   */
  protected function getNodeParagraphs(NodeInterface $node) {
    return $node->get('field_n_top')->referencedEntities();
  }

  /**
   * Loads the paragraphs from the configuration page.
   *
   * @param null|\Drupal\site_settings\Entity\SiteSettingEntity $settings
   *   The source site_settings entity.
   *
   * @return array
   *   The paragraphs render array.
   */
  protected function getSettingParagraphs($settings) {
    if (!($settings instanceof SiteSettingEntity)) {
      return NULL;
    }

    $paragraphs = $settings->get('field_ss_paragraphs')->referencedEntities();
    if (empty($paragraphs)) {
      return NULL;
    }

    return $paragraphs;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function getCacheTags() {
    /** @var \Drupal\node\Entity\Node $currentNode */
    $currentNode = $this->currentRequest->get('node');
    if ($currentNode instanceof Node && $currentNode->hasField('field_n_top')) {
      return Cache::mergeTags(parent::getCacheTags(), $currentNode->getCacheTags());
    }

    return parent::getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(
      parent::getCacheContexts(),
      ['route.ino_cc_node:field_n_top']
    );
  }

}
