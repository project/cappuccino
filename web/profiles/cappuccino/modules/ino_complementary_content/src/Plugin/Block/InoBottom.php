<?php

namespace Drupal\ino_complementary_content\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\ino_complementary_content\InoCustomBlock;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\site_settings\Entity\SiteSettingEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class InoBottom.
 *
 * @package Drupal\ino_complementary_content\Plugin\Block
 */
#[Block(id: 'bottom_block', admin_label: new TranslatableMarkup('Custom bottom content'), category: new TranslatableMarkup('Custom blocks'))]
class InoBottom extends InoCustomBlock {

  /**
   * The current request object.
   *
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * {@inheritdoc}
   */
  protected $siteSettingsType = 'ino_ss_bottom';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager, RequestStack $requestStack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $languageManager);
    $this->currentRequest = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function build() {
    $build = [];

    /** @var \Drupal\site_settings\Entity\SiteSettingEntity $bottom_settings */
    $bottom_settings = $this->loadSiteSettings();

    $output = $this->getParagraphs($bottom_settings);
    if (empty($output)) {
      return $build;
    }

    $build = [
      '#theme' => 'ino_bottom',
      '#paragraphs' => $this->getParagraphs($bottom_settings),
    ];

    return $build;
  }

  /**
   * Loads the paragraphs from the configuration page.
   *
   * @param null|\Drupal\site_settings\Entity\SiteSettingEntity $settings
   *   The source site_settings entity.
   *
   * @return array
   *   The paragraphs render array.
   */
  protected function getParagraphs($settings) {
    $paragraphs = [];

    if (NULL !== $settings) {
      $settings_paragraphs = $this->getSettingParagraphs($settings);
      if (is_array($settings_paragraphs)) {
        $paragraphs = array_merge($paragraphs, $settings_paragraphs);
      }
    }

    $view_builder = $this->entityTypeManager->getViewBuilder('paragraph');
    return $view_builder->viewMultiple($paragraphs ?: []);
  }

  /**
   * Loads the paragraphs from the site settings entity.
   *
   * @param null|\Drupal\site_settings\Entity\SiteSettingEntity $settings
   *   The source site_settings entity.
   *
   * @return array
   *   The paragraphs render array.
   */
  protected function getSettingParagraphs($settings) {
    if ($settings instanceof SiteSettingEntity) {
      $paragraphs = $settings->get('field_ss_paragraphs')->referencedEntities();
      if (!empty($paragraphs)) {
        return $paragraphs;
      }
    }
    return NULL;
  }

}
