<?php

namespace Drupal\ino_complementary_content\Cache;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Defines the complementary content site settings cache context service.
 *
 * Cache context ID: 'ino_cc_site_settings:%ss-type', e.g.
 * 'ino_cc_site_settings:ino_ss_bottom'.
 *
 * This allows for complementary content block cache location-aware caching.
 */
class InoComplementaryContentSiteSettingsCacheContext implements CalculatedCacheContextInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a new InoComplementaryContentSiteSettingsCacheContext service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The request stack.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t("Cappuccino complementary content block");
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function getContext($ss_type = NULL) {
    $entities = $this->entityTypeManager
      ->getStorage('site_setting_entity')
      ->loadByProperties(['type' => $ss_type]);

    if (empty($entities)) {
      return $ss_type . '.none';
    }

    /** @var \Drupal\site_settings\Entity\SiteSettingEntity $settings */
    $settings = array_shift($entities);
    return $ss_type . '.' . $settings->id();
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function getCacheableMetadata($ss_type = NULL) {
    $cacheable_metadata = new CacheableMetadata();
    /** @var \Drupal\node\Entity\Node $node */
    $entities = $this->entityTypeManager
      ->getStorage('site_setting_entity')
      ->loadByProperties(['type' => $ss_type]);

    if (!empty($entities)) {
      /** @var \Drupal\site_settings\Entity\SiteSettingEntity $settings */
      $settings = array_shift($entities);

      $cacheable_metadata->addCacheTags(['ino_ccss_id:' . $settings->id()]);
    }

    return $cacheable_metadata;
  }

}
