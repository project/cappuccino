<?php

namespace Drupal\ino_complementary_content\Cache;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CalculatedCacheContextInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines the complementary content node cache context service.
 *
 * Cache context ID: 'route.ino_cc_node:%field', e.g.
 * 'route.ino_cc_node:field_n_sidebar_left'.
 *
 * This allows for complementary content block cache location-aware caching.
 */
class InoComplementaryContentNodeCacheContext implements CalculatedCacheContextInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new InoComplementaryContentNodeCacheContext service.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t("Cappuccino complementary content block");
  }

  /**
   * {@inheritdoc}
   */
  public function getContext($field = NULL) {
    // Find the current node's ID.
    /** @var \Drupal\node\Entity\Node $node */
    if (($node = $this->requestStack->getCurrentRequest()->get('node')) && $node->hasField($field)) {
      if (!$node->get($field)->isEmpty()) {
        return $field . '.' . $node->id();
      }
    }

    return $field . '.none';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($field = NULL) {
    $cacheable_metadata = new CacheableMetadata();
    /** @var \Drupal\node\Entity\Node $node */
    if (($node = $this->requestStack->getCurrentRequest()->get('node')) && $node->hasField($field)) {
      // If the node has the field we can use the cache tag for that node.
      if (!$node->get($field)->isEmpty()) {
        $cacheable_metadata->addCacheTags(['ino_ccn_nid:' . $node->id()]);
      }
    }

    return $cacheable_metadata;
  }

}
