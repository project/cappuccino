<?php

namespace Drupal\ino_complementary_content;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\site_settings\Entity\SiteSettingEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class InoCustomBlock.
 *
 * @package Drupal\ino_complementary_content\Plugin\Block
 */
abstract class InoCustomBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The site settings type name.
   *
   * @var string
   */
  protected $siteSettingsType = '';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * Loads the given configuration page, and its fallback.
   *
   * @return null|\Drupal\site_settings\Entity\SiteSettingEntity
   *   The site_settings entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  protected function loadSiteSettings() {
    $current_langcode = $this->languageManager->getCurrentLanguage()->getId();

    $entities = $this->entityTypeManager
      ->getStorage('site_setting_entity')
      ->loadByProperties(['type' => $this->siteSettingsType]);

    if (empty($entities)) {
      return NULL;
    }

    /** @var \Drupal\site_settings\Entity\SiteSettingEntity $settings */
    $settings = array_shift($entities);

    if ($settings->hasTranslation($current_langcode)) {
      return $settings->getTranslation($current_langcode);
    }

    return $settings;
  }

  /**
   * Loads the paragraphs from the configuration page.
   *
   * @param null|\Drupal\site_settings\Entity\SiteSettingEntity $settings
   *   The source site_settings entity.
   *
   * @return array|null
   *   The paragraphs render array.
   */
  protected function getParagraphs($settings) {
    if (!($settings instanceof SiteSettingEntity)) {
      return NULL;
    }

    $paragraphs = $settings->get('field_ss_paragraphs')->referencedEntities();
    if (empty($paragraphs)) {
      return NULL;
    }

    $view_builder = $this->entityTypeManager->getViewBuilder('paragraph');
    return $view_builder->viewMultiple($paragraphs);
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function getCacheTags() {
    /** @var \Drupal\site_settings\Entity\SiteSettingEntity $settings */
    $site_settings = $this->loadSiteSettings();
    if (!empty($site_settings)) {
      return Cache::mergeTags(parent::getCacheTags(), $site_settings->getCacheTags());
    }

    return parent::getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(
      parent::getCacheContexts(),
      ['ino_cc_site_settings:' . $this->siteSettingsType]
    );
  }

}
