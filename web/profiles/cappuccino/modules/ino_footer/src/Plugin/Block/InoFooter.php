<?php

namespace Drupal\ino_footer\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\ino_complementary_content\InoCustomBlock;
use Drupal\site_settings\Entity\SiteSettingEntity;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class InoFooter.
 *
 * @package Drupal\ino_footer\Plugin\Block
 */
#[Block(id: 'footer_block', admin_label: new TranslatableMarkup('Custom footer'), category: new TranslatableMarkup('Custom blocks'))]
class InoFooter extends InoCustomBlock {

  /**
   * {@inheritdoc}
   */
  protected $siteSettingsType = 'ino_ss_footer';

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function build() {
    $build = [];

    /** @var \Drupal\site_settings\Entity\SiteSettingEntity $header_settings */
    $footer_settings = $this->loadSiteSettings();

    if (NULL === $footer_settings) {
      return $build;
    }

    $build = [
      '#theme' => 'ino_footer',
      '#body' => $this->getBody($footer_settings),
      '#cache' => [
        'tags' => $footer_settings->getCacheTags(),
      ],
    ];

    return $build;
  }

  /**
   * Loads the footer body from the configuration page.
   *
   * @param null|\Drupal\site_settings\Entity\SiteSettingEntity $settings
   *   The source site_settings entity.
   *
   * @return array
   *   The body render array.
   */
  protected function getBody($settings) {
    $body = [
      '#markup' => '',
    ];
    if (!($settings instanceof SiteSettingEntity)) {
      return $body;
    }

    $body_field = $settings->get('field_ss_body');
    if (empty($body_field) || empty($body_field->getValue()) || empty($body_field->getValue()[0]['value'])) {
      return $body;
    }

    $body['#markup'] = $body_field->getValue()[0]['value'];

    return $body;
  }

}
