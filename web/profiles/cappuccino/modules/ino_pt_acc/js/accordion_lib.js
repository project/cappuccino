(function ($) {
  "use strict";
  Drupal.applyAccordion = ( {element, item, header} )=> {

    const hash = window.location.hash;
    let activeAccordionIndex = false;
    if (hash.startsWith('#')) {
      element.find(item).each(function(index) {
        const search = hash.replace('#', '');
        const id = $(this).attr('id');
        if (id == search) {
          activeAccordionIndex = index;
          return false;
        }
      });
    }

    element.accordion({
      header: header,
      collapsible: true,
      active: activeAccordionIndex,
      heightStyle: "content",
      activate: function(event, ui) {
        if (typeof Drupal.KeyMetricsAnimate === 'function') {
          Drupal.KeyMetricsAnimate();
        }
        if (ui.newPanel.length && ui.oldPanel.length) {
          const newHeaderTop = ui.newHeader.offset().top;
          $('html, body').animate({ scrollTop: newHeaderTop - 50 }, 500);
        }
      },
      create: function(event, ui) {
        if (activeAccordionIndex !== false) {
          setTimeout(function() {
            const activeHeader = $(event.target).find(".ui-accordion-header-active");
            if (activeHeader.length) {
              const headerTop = activeHeader.offset().top;
              $('html, body').animate({ scrollTop: headerTop - 50 }, 500);
            }
          }, 100);
        }
      }
    });
    element.find(header).hover(
      function() {
        $(this).append('<span class="copy-link">¶</span>');

        $(this).find('.copy-link').click(function(event) {
          event.stopPropagation();
          event.preventDefault();

          const id = $(this).closest(item).attr('id');

          const urlWithHash = window.location.href.split('#')[0] + '#' + id;

          navigator.clipboard.writeText(urlWithHash).then(function() {
            const popup = $('<div class="copy-popup"></div>').text(Drupal.t('Link Copied!'));
            $('body').append(popup);
            popup.css({
              position: 'absolute',
              top: (event.pageY+15) + 'px',
              left: (event.pageX+15) + 'px',
              background: '#f9f9f9',
              border: '1px solid #ccc',
              padding: '5px',
              borderRadius: '5px',
              display: 'none',
              zIndex: 1000
            }).fadeIn(100).delay(500).fadeOut(500, function() {
              $(this).remove();
            });
          }).catch(function(err) {
            console.error('Could not copy URL: ', err);
          });
        });
      },
      function() {
        $(this).find('.copy-link').remove();
      }
    );
  };
})(jQuery);
