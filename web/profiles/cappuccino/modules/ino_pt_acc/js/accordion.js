(function ($) {
  "use strict";
  Drupal.behaviors.inoPtAccitem = {
    attach: function(context, settings) {
      const $elements = $(once('accordion-init', '.field--name-field-ph-acc-items', context));
      $elements.each(function() {
        Drupal.applyAccordion({
          element: $(this),
          item: '.paragraph--type--ino-pt-accitem',
          header: "div.paragraph--type--ino-pt-accitem > div.field--name-field-title"
        });
      });
    }
  };
})(jQuery);
