<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function ino_menu_form_menu_edit_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $menu = $form_state->getFormObject()->getEntity();

  if ($menu->id() == 'main') {
    $form['desktop_menu_len'] = [
      '#type' => 'number',
      '#title' => t('Desktop Menu Length'),
      '#default_value' => \Drupal::state()->get('desktop_menu_len', 4),
      '#weight' => -10,
    ];

    // Add a custom submit handler.
    if (isset($form['actions']['submit'])) {
      $form['actions']['submit']['#submit'][] = 'ino_menu_form_menu_edit_form_submit';
    }
  }
}

/**
 * Custom submit handler for menu_edit_form.
 */
function ino_menu_form_menu_edit_form_submit($form, FormStateInterface $form_state) {
  $desktop_menu_len = $form_state->getValue('desktop_menu_len');
  \Drupal::state()->set('desktop_menu_len', $desktop_menu_len);
}

/**
 * Implements hook_preprocess_HOOK().
 */
function ino_menu_preprocess_tb_megamenu(&$vars) {
  // Overriding global settings.
  $vars['content']['#block_config']['always-show-submenu'] = "0";
  $vars['block_config']['always-show-submenu'] = "0";

  if (!isset($vars['content']['#items'])) {
    return;
  }

  // Custom comparison function for sorting by weight.
  function _compare_by_weight($a, $b) {
    return $a->link->getWeight() - $b->link->getWeight();
  }

  // Sort items by weight.
  uasort($vars['content']['#items'], '_compare_by_weight');

  $limit = \Drupal::state()->get('desktop_menu_len', 4);
  $count = 0;

  foreach ($vars['content']['#items'] as &$item) {
    if (++$count > $limit) {
      $vars['content']['#menu_config'][$item->link->getPluginId()]['item_config']['class'] = 'mobile-only';
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function ino_menu_preprocess_tb_megamenu_item(array &$vars): void {
  $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
  if (!$vars['item']->link->hasTranslation($langcode)) {
    $vars['link'] = [];
  }
}
