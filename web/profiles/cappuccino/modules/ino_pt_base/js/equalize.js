/**
 * @file
 * JS for the Cappuccino base theme.
 */
(function ($, Drupal) {

  $(document).ready(function() {
    //Responsive tables
    $('table').wrap("<div class='table-responsive'></div>").once("tableresponsive");

    //Equalize height's cards
    function equalizeCard(paragraph) {
      var heightsArray = [];
      paragraph.find('.card-wrapper').each(function() {
        $(this).height('auto');
      });
      paragraph.find('.card-wrapper').each(function() {
        heightsArray.push($(this).height());
      });
      var highest = Math.max.apply(null, heightsArray);
      paragraph.find('.card-wrapper').each(function() {
        $(this).height(highest);
      });
    };
    $('.paragraph.equalize').once('equalize').each(function() {
      var paragraph = $(this);
      equalizeCard(paragraph);
      $(window).resize(function() {
        equalizeCard(paragraph);
      });
    });

  });

})(jQuery);
