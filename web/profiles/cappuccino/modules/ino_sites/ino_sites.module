<?php

/**
 * Loads the styles from the custom configuration file.
 *
 * @return array
 *   The configuration array.
 */
function _ino_sites_theme_colors() {
  if (\Drupal::hasService('ino_pt_helper.theme_configuration_provider')) {
    return \Drupal::service('ino_pt_helper.theme_configuration_provider')->getStyleOptions('themes');
  }
  return [];
}

/**
 * Converts hex to RGB values.
 */
function _ino_sites_hexToRgb($hex) {
  $hex = ltrim($hex, '#');

  if (strlen($hex) == 6) {
    $r = hexdec(substr($hex, 0, 2));
    $g = hexdec(substr($hex, 2, 2));
    $b = hexdec(substr($hex, 4, 2));
  } elseif (strlen($hex) == 3) {
    $r = hexdec(str_repeat(substr($hex, 0, 1), 2));
    $g = hexdec(str_repeat(substr($hex, 1, 1), 2));
    $b = hexdec(str_repeat(substr($hex, 2, 1), 2));
  } else {
    return null;
  }

  return "$r, $g, $b";
}

/**
 * Implements hook_preprocess_HOOK() for html.html.twig.
 */
function ino_sites_preprocess_html(&$variables) {
  // Load theme variables from state.
  $theme_variables = \Drupal::state()->get('theme_variables', []);

  $css_variables = [];

  if (!isset($theme_variables['global'])) return;

  // Add each theme variable as a CSS variable.
  foreach ($theme_variables['global'] as $name => $value) {
    $rgb_value = _ino_sites_hexToRgb($value);

    if ($rgb_value !== null) {
      // Create a safe CSS variable name by replacing any non-alphanumeric characters with a hyphen.
      $css_variable_name = '--' . preg_replace('/\W+/', '-', strtolower($name));

      $css_variables[] = "{$css_variable_name}: {$value}";
      $css_variables[] = "{$css_variable_name}_rgb: {$rgb_value}";
    }
  }

  // Combine all CSS variables into a single style tag.
  $variables['#attached']['html_head'][] = [
    [
      '#tag' => 'style',
      '#value' => ':root { ' . implode('; ', $css_variables) . '; }',
    ],
    'theme_css_variables',
  ];

  $variables['#attached']['library'][] = 'ino_sites/theme_colors';
}
