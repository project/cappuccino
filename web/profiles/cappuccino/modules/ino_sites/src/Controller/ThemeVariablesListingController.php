<?php

namespace Drupal\ino_sites\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller for listing theme variable groups.
 */
class ThemeVariablesListingController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ThemeVariablesListingController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Renders a list of theme variable groups including the 'global' group.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match interface.
   *
   * @return array
   *   A render array representing the listing page.
   */
  public function listingPage(RouteMatchInterface $route_match) {
    $links = [];

    // Add link for the 'Global' settings group.
    $links['global'] = Link::createFromRoute($this->t('Global'), 'ino_sites.theme_variables_form', ['group' => 'global'])->toRenderable();

    return [
      '#theme' => 'item_list',
      '#items' => $links,
    ];
  }
}
