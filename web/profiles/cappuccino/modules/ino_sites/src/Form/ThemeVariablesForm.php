<?php

namespace Drupal\ino_sites\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to manage theme variables.
 */
class ThemeVariablesForm extends FormBase {

  /**
   * The theme configuration provider service.
   *
   * @var \Drupal\ino_sites\ThemeConfigurationProvider
   */
  protected $themeConfigurationProvider;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->themeConfigurationProvider = $container->get('ino_pt_helper.theme_configuration_provider');
    $instance->state = $container->get('state');
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'theme_variables_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $group = NULL) {
    $stored_settings = $this->state->get('theme_variables')[$group] ?? [];
    $categories = $this->themeConfigurationProvider->getVariables();

    foreach ($categories as $category => $variables) {
      $form[$category] = [
        '#type' => 'details',
        '#title' => $this->t(ucwords(str_replace('_', ' ', $category))),
      ];

      foreach ($variables as $key => $item) {
        $default_value = isset($stored_settings[$key]) ? $stored_settings[$key] : ($item['default'] ?? '');

        $form[$category][$key] = [
          '#type' => $item['type'] ?? 'textfield',
          '#title' => $this->t($item['label']),
          '#default_value' => $default_value,
          '#description' => $item['desc'] ?? '',
        ];
      }
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    $form_state->set('group', $group);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $group = $form_state->get('group');

    $categories = $this->themeConfigurationProvider->getVariables();

    // Extract all variable keys.
    $theme_variables = [];
    foreach ($categories as $categoryVariables) {
      $theme_variables = array_merge($theme_variables, array_keys($categoryVariables));
    }

    // Filter the form values to only include theme variables.
    $filtered_values = array_intersect_key($values, array_flip($theme_variables));

    $all_settings = $this->state->get('theme_variables') ?? [];
    $all_settings[$group] = $filtered_values;

    $this->state->set('theme_variables', $all_settings);

    // Display success message.
    $this->messenger->addMessage($this->t('The configuration for the @group group has been successfully saved.', ['@group' => $group]));
  }
}
