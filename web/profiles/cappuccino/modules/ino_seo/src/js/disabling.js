/**
 * @file
 * Disabling teaser fields.
 */

(function ($, Drupal) {

  Drupal.behaviors.disabling = {
    attach: function (context, settings) {
      $(once('disabling', '#edit-field-n-auto-teaser-value', context)).click(function () {
        const ckeditorId = $('#edit-field-n-teaser-description-0-value').attr('data-ckeditor5-id');
        if (ckeditorId && Drupal.CKEditor5Instances && Drupal.CKEditor5Instances.has(ckeditorId)) {
          const editor = Drupal.CKEditor5Instances.get(ckeditorId);

          if ($(this).prop("checked") === true) {
            editor.enableReadOnlyMode('ckeditor5_disabled')
          } else {
            editor.disableReadOnlyMode('ckeditor5_disabled')
          }
        }
      });
    }
  };

})(window.jQuery, window.Drupal);
