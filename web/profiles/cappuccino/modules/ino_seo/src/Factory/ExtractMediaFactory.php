<?php

namespace Drupal\ino_seo\Factory;

use Drupal\ino_seo\Extractor\Media\ExtractorMediaCollection;
use Drupal\ino_seo\Extractor\Media\ExtractorMediaInterface;
use Drupal\ino_seo\Extractor\Media\ExtractorMediaMedia;
use Drupal\paragraphs\ParagraphInterface;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

/**
 * Class extract media entity from paragraph factory class.
 *
 * @package Drupal\ino_seo\Factory
 */
class ExtractMediaFactory implements ExtractMediaFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ParagraphInterface $paragraph): ExtractorMediaInterface {
    switch ($paragraph->getType()) {
      case 'ino_pt_accitem':
      case 'ino_pt_tabitem':
      case 'ino_pt_modal':
      case 'ino_pt_tab':
      case 'ino_pt_acc':
        /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
        return new ExtractorMediaCollection($paragraph);

      case 'ino_pt_media':
      case 'ino_pt_gallery':
        /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
        return new ExtractorMediaMedia($paragraph);

    }

    throw new InvalidArgumentException(\sprintf('%s paragraph type has no support', $paragraph->getType()));
  }

}
