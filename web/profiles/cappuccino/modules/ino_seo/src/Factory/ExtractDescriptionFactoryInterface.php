<?php

namespace Drupal\ino_seo\Factory;

use Drupal\ino_seo\Extractor\Description\ExtractorDescriptionInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Interface extract description from paragraph factory interface .
 *
 * @package Drupal\ino_seo\Factory
 */
interface ExtractDescriptionFactoryInterface {

  /**
   * Factory method to decorate queries with filters.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph entity.
   *
   * @return \Drupal\ino_seo\Extractor\Description\ExtractorDescriptionInterface
   *   A new formatter decorator class.
   */
  public static function createInstance(
    ParagraphInterface $paragraph
  ): ExtractorDescriptionInterface;

}
