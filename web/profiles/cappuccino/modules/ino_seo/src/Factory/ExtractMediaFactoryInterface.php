<?php

namespace Drupal\ino_seo\Factory;

use Drupal\ino_seo\Extractor\Media\ExtractorMediaInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Interface extract media entity from paragraph factory interface .
 *
 * @package Drupal\ino_seo\Factory
 */
interface ExtractMediaFactoryInterface {

  /**
   * Factory method to decorate queries with filters.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph entity.
   *
   * @return \Drupal\ino_seo\Extractor\Media\ExtractorMediaInterface
   *   A new formatter decorator class.
   */
  public static function createInstance(
    ParagraphInterface $paragraph
  ): ExtractorMediaInterface;

}
