<?php

namespace Drupal\ino_seo\Factory;

use Drupal\ino_seo\Extractor\Description\ExtractorDescriptionCollection;
use Drupal\ino_seo\Extractor\Description\ExtractorDescriptionInterface;
use Drupal\ino_seo\Extractor\Description\ExtractorDescriptionWysiwyg;
use Drupal\paragraphs\ParagraphInterface;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

/**
 * Class extract description from paragraph factory class.
 *
 * @package Drupal\ino_seo\Factory
 */
class ExtractDescriptionFactory implements ExtractDescriptionFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ParagraphInterface $paragraph): ExtractorDescriptionInterface {
    switch ($paragraph->getType()) {
      case 'ino_pt_wysiwyg':
        return new ExtractorDescriptionWysiwyg($paragraph);

      case 'ino_pt_accitem':
      case 'ino_pt_tabitem':
      case 'ino_pt_modal':
      case 'ino_pt_tab':
      case 'ino_pt_acc':
        return new ExtractorDescriptionCollection($paragraph);
    }

    throw new InvalidArgumentException(\sprintf('%s paragraph type has no support', $paragraph->getType()));
  }

}
