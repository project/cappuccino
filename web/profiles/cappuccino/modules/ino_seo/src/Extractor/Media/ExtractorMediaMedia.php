<?php

namespace Drupal\ino_seo\Extractor\Media;

use Drupal\media\MediaInterface;
use Exception;

/**
 * Class ExtractorMediaMedia.
 *
 * @package Drupal\ino_seo\Extractor\Media
 */
class ExtractorMediaMedia extends ExtractorMediaBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function readMedia(): MediaInterface {
    $field_names = $this->getFieldName($this->paragraph->getType());

    foreach ($field_names as $field_name) {
      /** @var \Drupal\media\MediaInterface $media_field */
      $media_field = $this->paragraph->get($field_name);
      /** @var \Drupal\media\MediaInterface $media_item */
      foreach ($media_field->referencedEntities() as $media_item) {
        if ($this->isMediaBundle($media_item->bundle())) {
          $media = $media_item;
          break;
        }
      }

      if (isset($media)) {
        break;
      }
    }

    if (isset($media)) {
      return $media;
    }

    throw new Exception($this->t('Media not found.'));
  }

}
