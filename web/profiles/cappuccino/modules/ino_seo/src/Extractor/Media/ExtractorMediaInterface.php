<?php

namespace Drupal\ino_seo\Extractor\Media;

use Drupal\ino_seo\Extractor\ExtractorTeaserInterface;
use Drupal\media\MediaInterface;

/**
 * Interface extract media interface.
 *
 * @package Drupal\ino_seo\Extractor\Media
 */
interface ExtractorMediaInterface extends ExtractorTeaserInterface {

  /**
   * Read media entity.
   *
   * @return \Drupal\media\MediaInterface
   *   The media entity.
   */
  public function readMedia(): MediaInterface;

}
