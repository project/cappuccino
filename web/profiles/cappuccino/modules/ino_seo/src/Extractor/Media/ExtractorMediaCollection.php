<?php

namespace Drupal\ino_seo\Extractor\Media;

use Drupal\paragraphs\ParagraphInterface;
use Drupal\media\MediaInterface;
use Exception;

/**
 * Class ExtractorMediaCollection.
 *
 * @package Drupal\ino_seo\Extractor\Media
 */
class ExtractorMediaCollection extends ExtractorMediaBase {

  /**
   * Extract media factory.
   *
   * @var \Drupal\ino_seo\Factory\ExtractMediaFactoryInterface
   */
  protected $extractMedia;

  /**
   * {@inheritdoc}
   */
  public function __construct(ParagraphInterface $paragraph) {
    parent::__construct($paragraph);

    $this->extractMedia = \Drupal::service('ino_seo.extract_media_factory');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function readMedia(): MediaInterface {
    $field_names = $this->getFieldName($this->paragraph->getType());

    foreach ($field_names as $field_name) {
      /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $paragraph_items */
      $paragraph_items = $this->paragraph->get($field_name);

      /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
      foreach ($paragraph_items->referencedEntities() as $paragraph) {
        /** @var \Drupal\ino_seo\Extractor\Media\ExtractorMediaInterface $em */
        try {
          $em = $this->extractMedia->createInstance($paragraph);
          $media = $em->readMedia();
        }
        catch (Exception $e) {
        }

        if (isset($media)) {
          break;
        }
      }

      if (isset($media)) {
        break;
      }
    }

    if (isset($media)) {
      return $media;
    }

    throw new Exception($this->t('Media not found.'));
  }

}
