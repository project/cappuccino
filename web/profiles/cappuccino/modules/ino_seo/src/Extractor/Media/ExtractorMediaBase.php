<?php

namespace Drupal\ino_seo\Extractor\Media;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\MediaInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Class ExtractorMediaBase.
 *
 * @package Drupal\ino_seo\Extractor\Media
 */
abstract class ExtractorMediaBase implements ExtractorMediaInterface {

  use StringTranslationTrait;

  /**
   * Enabled media bundles.
   *
   * @var array
   */
  protected const MEDIA_BUNDLES = ['image'];

  /**
   * Entity type to field name reference.
   */
  protected const REFERRER_FIELDS = [
    'ino_pt_accitem' => ['field_ph_accitem_content'],
    'ino_pt_tabitem' => ['field_ph_tabitem_content'],
    'ino_pt_modal' => ['field_ph_modal_content'],
    'ino_pt_media' => ['field_ph_media_reference'],
    'ino_pt_gallery' => ['field_ph_gallery_items'],
    'ino_pt_acc' => ['field_ph_acc_items'],
    'ino_pt_tab' => ['field_ph_tab_items'],
  ];

  /**
   * Paragraph entity.
   *
   * @var \Drupal\paragraphs\ParagraphInterface
   */
  protected $paragraph;

  /**
   * BaseFilterDecorator constructor.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph entity.
   */
  public function __construct(ParagraphInterface $paragraph) {
    $this->paragraph = $paragraph;
  }

  /**
   * {@inheritdoc}
   */
  public function read() {
    return $this->readMedia();
  }

  /**
   * {@inheritdoc}
   */
  abstract public function readMedia(): MediaInterface;

  /**
   * Validate media entity bundle.
   *
   * @param string $bundle
   *   Entity bundle.
   *
   * @return bool
   *   TRUE: Found in mediaBundles list.
   */
  protected function isMediaBundle(string $bundle): bool {
    if (in_array($bundle, self::MEDIA_BUNDLES, TRUE)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Get paragraph entity type's field name.
   *
   * @param string $bundle
   *   Paragraph entity bundle.
   *
   * @return null|array
   *   Field names.
   */
  protected function getFieldName(string $bundle) {
    return self::REFERRER_FIELDS[$bundle] ?? NULL;
  }

}
