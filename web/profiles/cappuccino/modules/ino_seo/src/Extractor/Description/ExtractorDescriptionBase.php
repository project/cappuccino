<?php

namespace Drupal\ino_seo\Extractor\Description;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\ino_seo\Extractor\Description\ExtractorDescriptionInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Class ExtractorDescriptionBase.
 *
 * @package Drupal\ino_seo\Extractor\Description
 */
abstract class ExtractorDescriptionBase implements ExtractorDescriptionInterface {

  /**
   * Entity type to field name reference.
   */
  protected const REFERRER_FIELDS = [
    'ino_pt_wysiwyg' => ['field_ph_wysiwyg_text'],
    'ino_pt_modal' => ['field_ph_modal_content'],
    'ino_pt_accitem' => ['field_ph_accitem_content'],
    'ino_pt_tabitem' => ['field_ph_tabitem_content'],
    'ino_pt_acc' => ['field_ph_acc_items'],
    'ino_pt_tab' => ['field_ph_tab_items'],
  ];


  /**
   * Paragraph entity.
   *
   * @var \Drupal\paragraphs\ParagraphInterface
   */
  protected $paragraph;

  /**
   * BaseFilterDecorator constructor.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph entity.
   */
  public function __construct(ParagraphInterface $paragraph) {
    $this->paragraph = $paragraph;
  }

  /**
   * {@inheritdoc}
   */
  public function read() {
    return $this->readDescription();
  }

  /**
   * {@inheritdoc}
   */
  abstract public function readDescription(): array;

  /**
   * Get paragraph entity type's field name.
   *
   * @param string $bundle
   *   Paragraph entity bundle.
   *
   * @return null|array
   *   Field names.
   */
  protected function getFieldName(string $bundle) {
    return self::REFERRER_FIELDS[$bundle] ?? NULL;
  }
}
