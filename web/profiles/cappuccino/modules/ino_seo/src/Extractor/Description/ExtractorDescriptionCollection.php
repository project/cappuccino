<?php

namespace Drupal\ino_seo\Extractor\Description;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\paragraphs\ParagraphInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ExtractorDescriptionCollection.
 *
 * @package Drupal\ino_seo\Extractor\Description
 */
class ExtractorDescriptionCollection extends ExtractorDescriptionBase {

  /**
   * Collection type list.
   *
   * @var array
   */
  protected $collectionTypes = [
    'ino_pt_modal',
    'ino_pt_acc',
    'ino_pt_accitem',
    'ino_pt_tab',
    'ino_pt_tabitem',
  ];

  /**
   * Extract description factory.
   *
   * @var \Drupal\ino_seo\Factory\ExtractDescriptionFactoryInterface
   */
  protected $extractDescription;

  /**
   * {@inheritdoc}
   */
  public function __construct(ParagraphInterface $paragraph) {
    parent::__construct($paragraph);
    $this->extractDescription = \Drupal::service('ino_seo.extract_description_factory');
  }

  /**
   * {@inheritdoc}
   */
  public function readDescription(): array {
    $description = [];
    $field_names = $this->getFieldName($this->paragraph->getType());
    foreach ($field_names as $field_name) {
      /** @var \Drupal\Core\Field\FieldItemListInterface $desc_field */
      $desc_field = $this->paragraph->get($field_name);

      foreach ($desc_field->referencedEntities() as $sparagraph) {
        $description = $this->extractDescriptionFromParagraph($sparagraph);
        if (isset($description['value']) && !empty($description['value'])) {
          break;
        }
      }

      if (!empty($description)) {
        break;
      }
    }

    return $description;
  }

  /**
   * Extracts a description from a given paragraph.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph entity.
   *
   * @return array
   *   The description and its format.
   */
  protected function extractDescriptionFromParagraph(ParagraphInterface $paragraph): array {
    $description = [];
    if ($this->isCollectionType($paragraph->getType())) {
      /** @var \Drupal\ino_seo\Extractor\Description\ExtractorDescriptionInterface $ed */
      $ed = $this->extractDescription->createInstance($paragraph);
      $description = $ed->readDescription();
    }

    return $description;
  }

  protected function isCollectionType(string $type) {
    if (in_array($type, $this->collectionTypes, FALSE)) {
      return TRUE;
    }

    return FALSE;
  }

}
