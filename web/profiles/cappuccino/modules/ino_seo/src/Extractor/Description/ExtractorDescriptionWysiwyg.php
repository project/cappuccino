<?php

namespace Drupal\ino_seo\Extractor\Description;

use Drupal\views\Plugin\views\field\FieldPluginBase;

/**
 * Class ExtractorDescriptionWysiwyg.
 *
 * @package Drupal\ino_seo\Extractor\Description
 */
class ExtractorDescriptionWysiwyg extends ExtractorDescriptionBase {

  /**
   * {@inheritdoc}
   */
  public function readDescription(): array {
    $description = [];

    $field_names = $this->getFieldName($this->paragraph->getType());

    foreach ($field_names as $field_name) {
      /** @var \Drupal\Core\Field\FieldItemListInterface $desc_field */
      $desc_field = $this->paragraph->get($field_name);

      if ($desc_field->isEmpty()) {
        continue;
      }

      $field_value = $desc_field->getValue()[0];
      $alter = [
        'max_length' => 250,
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'html' => TRUE,
      ];
      // Use FieldPluginBase::trimText() instead of Unicode::truncate() to
      // preserve html tags.
      $description['value'] = FieldPluginBase::trimText($alter, $field_value['value']);
      // Prevent unwanted new line.
      $description['value'] = str_replace('&#13;', '', $description['value']);
      $description['format'] = $field_value['format'];

      break;
    }

    return $description;
  }

}
