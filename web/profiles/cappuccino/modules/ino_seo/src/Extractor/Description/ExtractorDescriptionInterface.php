<?php

namespace Drupal\ino_seo\Extractor\Description;

use Drupal\ino_seo\Extractor\ExtractorTeaserInterface;

/**
 * Interface extract description interface.
 *
 * @package Drupal\ino_seo\Extractor\Description
 */
interface ExtractorDescriptionInterface extends ExtractorTeaserInterface {

  /**
   * Read description.
   *
   * @return array
   *   Description render array.
   */
  public function readDescription(): array;

}
