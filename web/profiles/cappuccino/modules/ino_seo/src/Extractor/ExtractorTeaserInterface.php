<?php

namespace Drupal\ino_seo\Extractor;

use Drupal\media\MediaInterface;

/**
 * Interface extract teaser field interface.
 *
 * @package Drupal\ino_seo\Extract
 */
interface ExtractorTeaserInterface {

  /**
   * Read teaser content.
   *
   * @return \Drupal\media\MediaInterface|array
   *   Media entity or description render array.
   */
  public function read();

}
