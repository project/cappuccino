<?php

namespace Drupal\ino_seo\Provider;

use Drupal\node\NodeInterface;
use Exception;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

/**
 * Class TokenDataProvider.
 *
 * @package Drupal\ino_seo
 */
class TokenDataProvider {

  /**
   * Extract description formatter.
   *
   * @var \Drupal\ino_seo\Factory\ExtractDescriptionFactoryInterface
   */
  protected $extractDescription;

  /**
   * Extract media formatter.
   *
   * @var \Drupal\ino_seo\Factory\ExtractMediaFactoryInterface
   */
  protected $extractMedia;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a TokenDataProvider object.
   */
  public function __construct() {
    $this->extractDescription = \Drupal::service('ino_seo.extract_description_factory');
    $this->extractMedia = \Drupal::service('ino_seo.extract_media_factory');
    $this->languageManager = \Drupal::service('language_manager');
  }

  /**
   * Extracts a description from the node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @return array
   *   The description and its format.
   */
  public function getPageDescription(NodeInterface $node) {
    $description = [];
    $field_name = 'field_n_paragraphs';

    $current_language = $this->languageManager->getCurrentLanguage()->getId();

    if ($node->hasField($field_name)) {
      /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $paragraphs */
      $paragraphs = $node->get($field_name);
      /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
      foreach ($paragraphs->referencedEntities() as $paragraph) {
        try {
          if ($paragraph->hasTranslation($current_language)) {
            $paragraph = $paragraph->getTranslation($current_language);
          }
          /** @var \Drupal\ino_seo\Extractor\Description\ExtractorDescriptionInterface $ed */
          $ed = $this->extractDescription->createInstance($paragraph);
          $description = $ed->read();
        }
        catch (InvalidArgumentException $e) {
        }

        if (isset($description['value']) && $description['value'] !== '') {
          break;
        }
      }
    }

    return $description;
  }

  /**
   * Extracts an image from the node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object.
   *
   * @return null|\Drupal\media\Entity\Media
   *   The media image object.
   */
  public function getPageImage(NodeInterface $node) {
    $media = NULL;

    $field_name = 'field_n_paragraphs';
    if ($node->hasField($field_name)) {
      /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $paragraphs */
      $paragraphs = $node->get($field_name);
      /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
      foreach ($paragraphs->referencedEntities() as $paragraph) {
        try {
          /** @var \Drupal\ino_seo\Extractor\Media\ExtractorMediaInterface $em */
          $em = $this->extractMedia->createInstance($paragraph);
          $media = $em->read();
        }
        catch (Exception $e) {
        }

        if (isset($media)) {
          break;
        }
      }
    }

    return $media;
  }

}
