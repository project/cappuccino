<?php

namespace Drupal\ino_content_editing\Entity;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\paragraphs\ParagraphsTypeInterface;

/**
 * Class ParagraphTypeUninstallValidator.
 *
 * @package Drupal\ino_content_editing\Entity
 */
class ParagraphTypeUninstallValidator implements ModuleUninstallValidatorInterface {
  use StringTranslationTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ContentUninstallValidator.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The string translation service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, TranslationInterface $stringTranslation) {
    $this->entityTypeManager = $entityTypeManager;
    $this->stringTranslation = $stringTranslation;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    $reasons = [];
    try {
      $entity_type = $this->entityTypeManager->getStorage('paragraphs_type')->load($module);
      if (!($entity_type instanceof ParagraphsTypeInterface)) {
        return $reasons;
      }
      $paragraphs = $this->entityTypeManager->getStorage('paragraph')->loadByProperties(['type' => $entity_type->id()]);
      if (count($paragraphs)) {
        $reasons[] = $this->t('There is content for the paragraph type: @entity_type. <a href=":url">Remove @entity_type</a>.', [
          '@entity_type' => $entity_type->label(),
          ':url' => Url::fromRoute('entity.paragraphs_type.delete_form', ['paragraphs_type' => $entity_type->id()])->toString(),
        ]);
      }
    }
    catch (\Exception $ignored) {
      return $reasons;
    }
    return $reasons;
  }

}
