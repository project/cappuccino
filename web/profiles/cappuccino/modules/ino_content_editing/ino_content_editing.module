<?php

/**
 * @file
 * Contains hook implementations.
 */

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\layout_paragraphs\LayoutParagraphsLayout;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\field\Entity\FieldConfig;

/**
 * Client project field prefix.
 */
define('INO_CONTENT_EDITING_FIELD_NAME_PREFIX', '');

/**
 * Implements hook_preprocess_HOOK() for html.html.twig.
 */
function ino_content_editing_preprocess_html(&$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = \Drupal::routeMatch()->getParameter('node');
  if ($node instanceof Node && $node->hasField('field_n_body_classes') && !$node->get('field_n_body_classes')->isEmpty()) {
    $custom_classes = explode(' ', $node->get('field_n_body_classes')->value);
    foreach ($custom_classes as $class) {
      $class = Html::cleanCssIdentifier($class);
      if (!empty($class)) {
        $variables['attributes']['class'][] = $class;
      }
    }
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 *
 * Gets the paragraph entity from a 'paragraphs' widget type.
 */
function ino_content_editing_field_widget_get_paragraph(array $element, FormStateInterface $form_state, array $context) {
  /** @var Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $items */
  $items = $context['items'];
  $field_name = $items->getName();
  $parents = array_merge(
    ['field_storage', '#parents'],
    $element['#field_parents'],
    ['#fields', $field_name, 'paragraphs', 0, 'entity']
  );
  /** @var Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = NestedArray::getValue($form_state->getStorage(), $parents);
  return $paragraph;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Alter the Layout Paragraph builder form.
 *
 * @param array $form
 *   The Layout Paragraph builder form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function ino_content_editing_form_layout_paragraphs_builder_form_alter(array &$form, FormStateInterface $form_state) {
  if (isset($form['layout_paragraphs_builder_ui']['#layout_paragraphs_layout']) && $form['layout_paragraphs_builder_ui']['#layout_paragraphs_layout'] instanceof LayoutParagraphsLayout) {
    $layout_paragraphs_layout = $form['layout_paragraphs_builder_ui']['#layout_paragraphs_layout'];

    $paragraphs_reference_field = $layout_paragraphs_layout->getParagraphsReferenceField();

    if ($paragraphs_reference_field && $paragraphs_reference_field instanceof EntityReferenceRevisionsFieldItemList) {
      $field_definition = $paragraphs_reference_field->getFieldDefinition();

      if ($field_definition instanceof FieldConfig) {
        $field_name = $field_definition->getName();
        if (!empty($field_name) && isset($form['#attributes']['class'])) {
          $form['#attributes']['class'][] = 'field-name-' . $field_name;
        }
      }
    }
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 *
 * Widget type : Paragraphs EXPERIMENTAL : paragraphs.
 *
 * Not called on Paragraphs Edit forms but that's ok for new content.
 *
 * @see https://www.drupal.org/project/paragraphs_tools
 */
function ino_content_editing_field_widget_paragraphs_form_alter(&$element, FormStateInterface $form_state, $context) {
  /** @var array $form */
  $form = $context['form'];
  /** @var Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget $widget */
  $widget = $context['widget'];
  /** @var Drupal\Core\Field\FieldItemListInterface $items */
  $items = $context['items'];
  /** @var int $delta */
  $delta = $context['delta'];
  /** @var Drupal\Core\Entity\Entity\EntityFormDisplay $form_display */
  $build_info = $form_state->getBuildInfo();
  if ('node_form' === $build_info['base_form_id']) {
    /** @var Drupal\node\Entity\Node $node */
    $node = $form_state->getFormObject()->getEntity();
    $triggering_element = $form_state->getTriggeringElement();
    if ($node->isNew() && is_null($triggering_element)) {
      // Open default paragraphs on node create form.
      /** @var \Drupal\field\Entity\FieldConfig $field_definition */
      $field_definition = $context['items']->getFieldDefinition();
      $field_name = $field_definition->getName();
      // @see \Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget::formElement().
      $widget_state = WidgetBase::getWidgetState($element['#field_parents'], $field_name, $form_state);
      if ('closed' === $widget_state['paragraphs'][$delta]['mode']) {
        // This is a closed default paragraph,
        // see ino_content_editing_node_prepare_form().
        $widget_state['paragraphs'][$delta]['mode'] = 'edit';
        WidgetBase::setWidgetState($element['#field_parents'], $field_name, $form_state, $widget_state);
        // Recreate the field widget with the new settings.
        $keys_to_preserve = [
          '#title',
          '#description',
          '#field_parents',
          '#required',
          '#delta',
          '#weight',
        ];
        $element = array_intersect_key($element, array_fill_keys($keys_to_preserve, TRUE));
        $element = $widget->formElement($items, $delta, $element, $form, $form_state);
      }
    }
  }
}

/**
 * Gets the next weight in a form.
 *
 * @param array $form
 *   Form render array.
 * @param array $skip_keys
 *   Form elements to skip.
 *
 * @return int
 *   Next weight.
 */
function ino_content_editing_get_form_next_weight(array $form, array $skip_keys = ['actions']) : int {
  $weight = 0;
  $keys = Element::getVisibleChildren($form);
  foreach ($keys as $key) {
    if (isset($form[$key]['#weight']) && $form[$key]['#weight'] >= $weight && !in_array($key, $skip_keys)) {
      $weight = $form[$key]['#weight'];
    }
  }

  return $weight + 1;
}

/**
 * Adds paragraph item to field in entity.
 *
 * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
 *   Fieldable entity.
 * @param string $paragraph_type
 *   Paragraph bundle name.
 * @param string $field_name
 *   Field name.
 *
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 *   Thrown if the entity type doesn't exist.
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 *   Thrown if the storage handler couldn't be loaded.
 */
function ino_content_editing_append_paragraph(FieldableEntityInterface $entity, $paragraph_type, $field_name = 'field_n_paragraphs') {
  $paragraph_storage = &drupal_static(__FUNCTION__);
  if (!isset($paragraph_storage)) {
    $paragraph_storage = \Drupal::entityTypeManager()->getStorage('paragraph');
  }
  $paragraph = $paragraph_storage->create([
    'type' => $paragraph_type,
  ]);
  $entity->get($field_name)->appendItem($paragraph);
}

/**
 * Implements hook_preprocess_HOOK().
 *
 * {@inheritdoc}
 */
function ino_content_editing_preprocess_paragraphs_add_dialog(&$variables) {
  $new_buttons = [];
  foreach ($variables['buttons'] as $button) {
    $button['#attributes']['class'][] = str_replace('_', '-', $button['#bundle_machine_name']);
    $new_buttons[] = $button;
  }
  $variables['buttons'] = $new_buttons;
}

/**
 * Set dynamic allowed values for the layout field.
 *
 * @param \Drupal\field\Entity\FieldStorageConfig $definition
 *   The field definition.
 * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
 *   The entity being created if applicable.
 * @param array $options
 *   Options to either exclude or restrict layouts (by provider/module names).
 *
 * @return array
 *   An array of possible key and value options.
 *
 * @see options_allowed_values()
 */
function ino_content_editing_layout_allowed_values(FieldStorageConfig $definition, ContentEntityInterface $entity = NULL, array $options = []) {
  $restrict_providers = [];
  $exclude_providers = [];
  if (isset($options['restrict'])) {
    // Flipping array for fast isset() checks.
    $restrict_providers = array_flip($options['restrict']);
  }
  elseif (isset($options['exclude'])) {
    $exclude_providers = array_flip($options['exclude']);
  }
  $layout_definitions = \Drupal::service('plugin.manager.core.layout')->getDefinitions();
  $defined_layouts = [];

  /**
   * @var Drupal\Core\Layout\LayoutDefinition $layout_definition
   */
  foreach ($layout_definitions as $layout_definition) {
    $provider = $layout_definition->getProvider();
    if ((empty($restrict_providers) || isset($restrict_providers[$provider])) && !isset($exclude_providers[$provider])) {
      $defined_layouts[$layout_definition->id()] = $layout_definition->getLabel();
    }
  }

  return $defined_layouts;
}


/**
 * Implements hook_ENTITY_TYPE_presave().
 *
 * Invalidates custom cache tags when a node is created or updated.
 */
function ino_content_editing_node_presave(NodeInterface $node) {
  ino_content_editing_clear_custom_node_cache($node->getType());
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 *
 * Invalidates custom cache tags when a node is deleted.
 */
function ino_content_editing_node_delete(NodeInterface $node) {
  ino_content_editing_clear_custom_node_cache($node->getType());
}

/**
 * Clears custom node_list cache tags.
 *
 * @param string $node_type
 *   The node bundle.
 *
 * @see views_custom_cache_tag
 */
function ino_content_editing_clear_custom_node_cache(string $node_type) {
  $tags = ['ino_content_editing:node_list:' . $node_type];
  Cache::invalidateTags($tags);
}

/**
 * Full path to a (sub)form element for Form API #states.
 *
 * @param array $subform
 *   Subform array.
 * @param string $field_name
 *   Field name.
 * @param string $column_name
 *   Column name for the value, optional.
 *
 * @return string
 *   The full path string.
 */
function ino_content_editing_form_element_states_path(array $subform, $field_name, $column_name = '') {
  if (!isset($subform[$field_name])) {
    return '';
  }
  $element_path = $subform[$field_name]['#parents'];
  // Remove "$field_name_wrapper".
  array_pop($element_path);
  if (empty($element_path)) {
    // Paragraphs Edit form.
    $element_path = $field_name;
  }
  else {
    // Edit page subform.
    $element_path = array_shift($element_path) . '[' . implode('][', $element_path) . '][' . $field_name . ']';
  }
  if (!empty($column_name)) {
    $element_path .= "[$column_name]";
  }
  return $element_path;
}

/**
 * Implements hook_form_alter().
 */
function ino_content_editing_form_alter(&$form, &$form_state, $form_id) {
  // Add style for paragraph widget.
  $form['#attached']['library'][] = 'ino_content_editing/backoffice';

  // Add format options to link dialog in ckeditor.
  if (strpos($form_id, 'editor_link_dialog') !== FALSE) {
    unset($form['attributes']['class']['#group']);
    $form['attributes']['class']['#title'] = t('style');
    $form['attributes']['class']['#type'] = 'select';
    $form['attributes']['class']['#options'] = [
      'none' => t('- none -'),
      'btn' => t('button'),
    ];
    $form['attributes']['class']['#weight'] = 2;
    $form['advanced']['#weight'] = 3;
  }

  if (!empty($form['field_n_paragraphs'])) {
    unset($form['field_n_paragraphs']);
    $form['content'] = [
      '#type' => 'details',
      '#title' => t('Content'),
      '#weight' => 80,
      '#open' => TRUE,
    ];
    $form['content']['field_n_paragraphs'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . t('You can edit the main content of the page on the "View" tab after saving the form.') . '</p>',
      '#prefix' => '<div class="field-n-paragraphs-message">',
      '#suffix' => '</div>',
    ];
    $form['content']['field_n_paragraphs_image'] = [
      '#type' => 'markup',
      '#markup' => '<img src="/' . \Drupal::service('extension.list.module')->getPath('ino_content_editing') . '/assets/group32.png" alt="directions for editing content" class="field-n-paragraphs-image">',
      '#prefix' => '<div class="field-n-paragraphs-image-wrapper">',
      '#suffix' => '</div>',
    ];
  }

  // Alter node forms to have a better UX.
  $form_ids = [
    'node_ino_page_form',
    'node_ino_page_edit_form',
    'node_ino_news_form',
    'node_ino_news_edit_form',
  ];
  if (in_array($form_id, $form_ids)) {
    // Move language field in the right block.
    if (!empty($form['langcode'])) {
      $form['language'] = [
        '#type' => 'container',
        '#title' => t('Language'),
        '#group' => 'advanced',
        '#tree' => TRUE,
        '#access' => TRUE,
        '#weight' => -20,
        '#attributes' => [
          'class' => ['entity-meta__header'],
        ],
        'langcode' => $form['langcode'],
      ];
      unset($form['langcode']);
    }

    // Move body class field in the right block.
    if (!empty($form['field_n_body_classes'])) {
      $form['body_classes'] = [
        '#type' => 'details',
        '#title' => t('Body class'),
        '#group' => 'advanced',
        '#open' => FALSE,
        '#weight' => 0,
        'field_n_body_classes' => $form['field_n_body_classes'],
      ];
      unset($form['field_n_body_classes']);
    }

    if (!empty($form['field_n_tags'])) {
      // Put all taxonomy fields in a collapsed fieldset.
      $form['taxonomies'] = [
        '#type' => 'details',
        '#title' => t('Taxonomies'),
        '#open' => TRUE,
        '#group' => 'advanced',
        '#weight' => -15,
      ];
      if (!empty($form['field_n_tags'])) {
        $form['taxonomies']['taxo_tags'] = $form['field_n_tags'];
        unset($form['field_n_tags']);
      }
    }

    if (!empty($form['field_n_body_classes'])) {
      // Hide body class(es) field to non admin users.
      $roles = \Drupal::currentUser()->getRoles();
      if (!in_array('administrator', $roles)) {
        $form['field_n_body_classes']['#access'] = FALSE;
      }
    }

  }
}
