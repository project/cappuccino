<?php

namespace Drupal\ino_header\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Cache\Cache;
use Drupal\file\Entity\File;
use Drupal\ino_complementary_content\InoCustomBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\site_settings\Entity\SiteSettingEntity;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class InoHeader.
 *
 * @package Drupal\ino_header\Plugin\Block
 */
#[Block(id: 'header_block', admin_label: new TranslatableMarkup('Custom header'), category: new TranslatableMarkup('Custom blocks'))]
class InoHeader extends InoCustomBlock {

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  protected $siteSettingsType = 'ino_ss_header';

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Route Match Interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->languageManager = $container->get('language_manager');
    $instance->routeMatch = $container->get('current_route_match');
    $instance->requestStack = $container->get('request_stack');
    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function build() {
    $build = [];

    /** @var \Drupal\site_settings\Entity\SiteSettingEntity $header_settings */
    $header_settings = $this->loadSiteSettings();

    if (NULL !== $header_settings) {
      $build = [
        '#theme' => 'ino_header',
        '#logo' => $this->getLogo($header_settings),
        '#logo_url' => $this->getLogoUrl(),
        '#site_name' => $this->getSiteName($header_settings),
        '#links' => $this->getSiteSwitcherLinks($header_settings),
        '#language_switcher' => $this->getLanguageSwitcher(),
        '#cache' => [
          'tags' => $header_settings->getCacheTags(),
        ],
      ];
    }

    return $build;
  }

  /**
   * Creates the language switcher.
   *
   * @return array
   *   The language switcher render array.
   */
  protected function getLanguageSwitcher(): array {
    $links = [];
    $route_name = $this->routeMatch->getRouteName();
    $node = $this->routeMatch->getParameter('node');

    foreach ($this->languageManager->getLanguages() as $langcode => $language) {

      if ($node && $node->hasTranslation($langcode)) {
        $translated_node = $node->getTranslation($langcode);
        $url = $translated_node->toUrl();
      }
      elseif (empty($node)) {
        /** @var \Symfony\Component\HttpFoundation\InputBag $param_bag */
        $param_bag = $this->routeMatch->getRawParameters();

        $current_request = $this->requestStack->getCurrentRequest();
        $current_query_params = $current_request->query->all();

        $url = Url::fromRoute($route_name, $param_bag->all(), ['language' => $langcode]);
        $url->setOption('query', $current_query_params);
      }
      else {
        $url = Url::fromRoute('<front>');
      }

      $link_classes = [];
      if ($langcode === $this->languageManager->getCurrentLanguage()->getId()) {
        $link_classes[] = 'is-active';
      }

      $links[$langcode] = [
        'title' => strtoupper($langcode),
        'url' => $url,
        'language' => $language,
        'attributes' => ['class' => $link_classes],
      ];
    }

    return [
      '#theme' => 'links__language_block',
      '#links' => $links,
      '#attributes' => ['class' => ['links']],
    ];
  }

  /**
   * Loads the logo url.
   */
  protected function getLogoUrl() {
    return Url::fromRoute('<front>')->toString();
  }

  /**
   * Loads the logo file from the configuration page.
   *
   * @param \Drupal\site_settings\Entity\SiteSettingEntity|null $settings
   *   The source site_settings entity.
   *
   * @return array
   *   The file render array.
   */
  protected function getLogo(?SiteSettingEntity $settings): ?array {
    if (!$settings instanceof SiteSettingEntity) {
      return NULL;
    }
    $logo = $settings->get('field_ss_logo')->entity;
    if (!$logo instanceof File) {
      return NULL;
    }

    $file_uri = $logo->getFileUri();

    $file_extension = pathinfo($file_uri, PATHINFO_EXTENSION);

    if (strtolower($file_extension) === 'svg') {
      return [
        '#theme' => 'image',
        '#uri' => $file_uri,
      ];
    }

    return [
      '#theme' => 'image_style',
      '#style_name' => 'thumbnail',
      '#uri' => $file_uri,
    ];
  }

  /**
   * Loads the site name from the configuration page.
   *
   * @param \Drupal\site_settings\Entity\SiteSettingEntity|null $settings
   *   The source site_settings entity.
   *
   * @return array
   *   The site name render array.
   */
  protected function getSiteName(?SiteSettingEntity $settings): array {
    $title = [
      '#markup' => '',
    ];
    if (!($settings instanceof SiteSettingEntity)) {
      return $title;
    }

    $name_field = $settings->get('field_ss_sitename');
    if (empty($name_field) || empty($name_field->getValue()) || empty($name_field->getValue()[0]['value'])) {
      return $title;
    }

    $title['#markup'] = $name_field->getValue()[0]['value'];

    return $title;
  }

  /**
   * Loads the site switcher links from the configuration page.
   *
   * @param null|\Drupal\site_settings\Entity\SiteSettingEntity $settings
   *   The source site_settings entity.
   *
   * @return array
   *   The site switcher links render array.
   */
  protected function getSiteSwitcherLinks($settings) {
    $markup = [
      '#markup' => '',
    ];
    if (!($settings instanceof SiteSettingEntity)) {
      return $markup;
    }

    /** @var \Drupal\Core\Field\FieldItemList $switcher_field */
    $switcher_field = $settings->get('field_ss_site_switcher_link');
    if (NULL === $switcher_field || 0 === $switcher_field->count()) {
      return $markup;
    }

    return $switcher_field->view('default');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(
      parent::getCacheContexts(),
      ['url.path']
    );
  }

}
