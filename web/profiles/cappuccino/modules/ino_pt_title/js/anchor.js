(function () {
  "use strict";

  Drupal.behaviors.inoTitleAnchor = {
    attach: function (context, settings) {
      const elements = once('anchor-init', '.sectionlink .title-pt-main-link', context);
      elements.forEach(function (element) {
        const titleElement = element.querySelector('.title-pt-title');

        titleElement.addEventListener('mouseenter', function () {
          const copyLink = document.createElement('span');
          copyLink.classList.add('copy-link');
          copyLink.textContent = '¶';
          titleElement.appendChild(copyLink);

          copyLink.addEventListener('click', function (event) {
            event.stopPropagation();
            event.preventDefault();

            const id = element.getAttribute('id');
            const urlWithHash = window.location.href.split('#')[0] + '#' + id;

            navigator.clipboard.writeText(urlWithHash).then(function () {
              console.log('URL copied to clipboard');
            }).catch(function (err) {
              console.error('Could not copy URL: ', err);
            });
          });
        });

        titleElement.addEventListener('mouseleave', function () {
          const copyLink = titleElement.querySelector('.copy-link');
          if (copyLink) {
            titleElement.removeChild(copyLink);
          }
        });
      });
    }
  };
})();
