(function () {
  "use strict";

  Drupal.behaviors.inoTitleResponsive = {
    attach: function (context, settings) {
      const elements = once('responsive-init', '.title-pt-background', context);
      elements.forEach(function (element) {
        const picture = element.querySelector('picture');
        if (picture) {
          const img = picture.querySelector('img');

          if (img) {
            const updateBackgroundImage = function () {
              const imageSrc = img.currentSrc;
              const currentBackgroundImage = getComputedStyle(element).backgroundImage;
              const currentBackgroundUrl = currentBackgroundImage.replace(/url\(["']?/, '').replace(/["']?\)$/, '');

              if (currentBackgroundUrl !== imageSrc) {
                element.style.backgroundImage = `url(${imageSrc})`;
              }
            };

            img.addEventListener('load', updateBackgroundImage);

            if (img.complete && img.naturalWidth > 0) {
              updateBackgroundImage();
            }
          }
        }
      });
    }
  };
})();
