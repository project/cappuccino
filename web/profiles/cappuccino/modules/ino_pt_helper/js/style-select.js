(function ($, Drupal, once) {
  "use strict";

  Drupal.behaviors.inoStyleSelect = {
    attach: function (context) {
      const $elements = $(once('inoStyleSelect', '.ino-pt-style-widget', context));
      $elements.each(function () {
        const $widget = $(this);

        // Set icon from default value:
        let def_value = $('select', $widget).val();
        def_value = Array.isArray(def_value) ? def_value : [def_value];

        if (def_value.length) {
          def_value.forEach(function (item) {
            const $group_label = $('select option[value="' + item + '"]', $widget).parent().attr('label');
            $('.style-group[data-group-value="' + $group_label + '"] li.style-item[data-item-value="' + item + '"]', $widget).addClass('active');
          });
        }

        // Show widget:
        $widget.show();

        // Set value in select options:
        $widget.find('.style-item').click(function () {
          $(this).parents('.style-items').find('.style-item').removeClass('active');
          $(this).addClass('active');

          let style_values = [];
          $widget.find('.style-item.active').each(function (item, key) {
            style_values.push($(this).attr('data-item-value'));
          });

          const wrapperID = $widget.attr('id');
          $('#' + wrapperID + '.ino-pt-style-widget select').val(style_values);
        });
      });
    }
  };
})(jQuery, Drupal, once);
