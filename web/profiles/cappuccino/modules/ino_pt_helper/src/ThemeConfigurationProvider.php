<?php

namespace Drupal\ino_pt_helper;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ThemeConfigurationProvider.
 *
 * @package Drupal\ino_basetheme_helper
 */
class ThemeConfigurationProvider {

  use StringTranslationTrait;

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The configuration file contents.
   *
   * @var array
   */
  protected $configuration;

  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The file system object.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  private $fileSystem;

  /**
   * ThemeConfigurationProvider constructor.
   *
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The theme handler service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration object factory.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The file system object.
   */
  public function __construct(ThemeHandlerInterface $themeHandler, ConfigFactoryInterface $config_factory, FileSystem $file_system) {
    $this->themeHandler = $themeHandler;
    $this->configFactory = $config_factory;
    $this->fileSystem = $file_system;
    $this->loadConfiguration();
  }

  /**
   * Reads the configuration file(s).
   */
  private function loadConfiguration() {
    $theme_name = $this->configFactory->get('system.theme')->get('default');
    /** @var \Drupal\Core\Extension\Extension $theme */
    $theme = $this->themeHandler->getTheme($theme_name);
    $theme_names = array_keys($theme->base_themes);
    // Add default theme name after it's base themes.
    $theme_names[] = $theme_name;
    $installed_themes = $this->themeHandler->listInfo();
    $this->configuration = NULL;
    foreach (array_reverse($theme_names) as $theme_name) {
      if (empty($installed_themes[$theme_name])) {
        continue;
      }
      $theme = $this->themeHandler->getTheme($theme_name);
      $theme_path = $theme->getPath();
      $config_file_name = $theme_name . '_configuration.yml';
      if (is_dir("$theme_path/config/custom")) {
        $files = $this->fileSystem->scanDirectory("$theme_path/config/custom", '/^' . $config_file_name . '$/');
        $file = reset($files);
      }

      if (!empty($file)) {
        $yml = Yaml::parseFile($file->uri);
        $this->configuration = $yml;
        // Stop after the first theme with configuration.
        break;
      }
    }
  }

  /**
   * Provides configuration arrays.
   *
   * @param string $key
   *   The configuration array key.
   *
   * @return mixed|null
   *   The array value, if any.
   */
  public function get($key) {
    if (!empty($this->configuration[$key])) {
      $styles = $this->configuration[$key];
      array_walk_recursive($styles, function(&$value, $key) {
        if ($key == 'title') {
          $value = $this->t($value);
        }
      });
      return $styles;
    }
    return NULL;
  }

  /**
   * Provides the styles used for the PTs.
   *
   * @return array
   *   The styles array.
   */
  public function getStyles() {
    return $this->get('styles');
  }

  /**
   * Provides the variables used for the theme customization.
   *
   * @return array
   *   The variables array.
   */
  public function getVariables() {
    return $this->get('variables');
  }

  /**
   * Provides the style used for the PTs.
   *
   * @param $key
   *   PT type.
   *
   * @return array
   *   The styles array.
   */
  public function getStyle($key) {
    $styles = $this->get('styles');

    if (isset($styles[$key])) {
      return $styles[$key];
    }

    return [];
  }

  /**
   * Provides the style options used for the PTs.
   *
   * @param $key
   *   PT type.
   *
   * @return array
   *   The styles array.
   */
  public function getStyleOptions($key) {
    $styles = $this->getStyle($key);
    $options = [];

    if (!empty($styles)) {
      $options = [];
      foreach ($styles as $group_key => $style_group) {
        $group_name = (string) $style_group['title'];
        foreach ($style_group['style'] as $key => $style) {
          $options[$group_name][$key] = $style['title'];
        }
      }
    }

    return $options;
  }

}
