<?php

namespace Drupal\ino_pt_helper\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ThemeHandler;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ino_pt_helper\ThemeConfigurationProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'options_select' widget.
 */
#[FieldWidget(id: 'style_select', label: new TranslatableMarkup('Style select'), field_types: ['list_string'], multiple_values: true)]
class StyleSelectWidget extends OptionsSelectWidget {

  /**
   * Theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandler
   */
  private $themeHandler;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * @var \Drupal\ino_pt_helper\ThemeConfigurationProvider
   */
  private $themeConfigurationProvider;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ThemeHandler $theme_handler, ConfigFactoryInterface $config_factory, ThemeConfigurationProvider $ino_pt_theme_configuration) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->themeHandler = $theme_handler;
    $this->configFactory = $config_factory;
    $this->themeConfigurationProvider = $ino_pt_theme_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('theme_handler'),
      $container->get('config.factory'),
      $container->get('ino_pt_helper.theme_configuration_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['options'] = parent::formElement($items, $delta, $element, $form, $form_state);

    $entity = $items->getEntity();
    if ($entity !== NULL) {
      if ($entity instanceof TermInterface) {
        $type = $entity->bundle();
      }
      else {
        $type = $entity->getType();
      }
      $style_config = $this->themeConfigurationProvider->getStyle($type);
    }

    if (isset($style_config)) {
      $config_default = [];
      $defaultThemeName = $this->configFactory->get('system.theme')->get('default');
      /** @var \Drupal\Core\Extension\Extension $default_theme */
      $default_theme = $this->themeHandler->getTheme($defaultThemeName);
      $default_theme_path = '/' . $default_theme->getPath();
      foreach ($style_config as $group_key => $group_item) {
        if (isset($group_item['asset'])) {
          $style_config[$group_key]['asset'] = $default_theme_path . '/assets/select-style/' . $group_item['asset'];
        }

        if (isset($group_item['class'])) {
          $style_config[$group_key]['class'] = $group_item['class'];
        }

        // Set default value in config:
        if (isset($group_item['default'])) {
          if (isset($group_item['style'][$group_item['default']])) {
            $config_default[$group_key] = $group_item['default'];
          }
        }

        if (isset($group_item['style'])) {
          foreach ($group_item['style'] as $key => $style) {
            if (isset($style['asset'])) {
              $style_config[$group_key]['style'][$key]['asset'] = $default_theme_path . '/assets/select-style/' . $style['asset'];
            }

            // If empty default config then set first item:
            if (!isset($config_default[$group_key])) {
              $config_default[$group_key] = $key;
            }
          }
        }
      }

      $element['#type'] = 'container';
      $element['#attached'] = [
        'library' => ['ino_pt_helper/style-select'],
      ];
      $element += ['#attributes' => []];
      $element['#attributes'] += ['class' => []];
      $element['#attributes']['class'][] = 'ino-pt-style-widget';
      $element['#attributes']['class'][] = 'hidden';

      $element['style-widget'] = [
        '#theme' => 'ino_pt_style_widget',
        '#styles' => $style_config,
      ];

      $default_options = $this->getSelectedOptions($items);
      if (empty($default_options)) {
        $element['options']['#default_value'] = $config_default;
      }
    }

    $element['options'] += ['#attributes' => []];
    $element['options']['#attributes'] += ['class' => []];
    $element['options']['#attributes']['class'][] = 'hidden';

    // Put element in fieldset.
    $widget = $element;
    $widget['options']['#title_display'] = 'invisible';
    $element = [
      '#type' => 'details',
      '#title' => $this->t('Style'),
      '#open' => FALSE,
      '#field_parents' => $widget['#field_parents'],
      'widget' => $widget,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();

    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name, 'widget', 'options']);
    $key_exists = NULL;
    $values = NestedArray::getValue($form_state->getValues(), $path, $key_exists);

    if ($key_exists) {
      // Account for drag-and-drop reordering if needed.
      if (!$this->handlesMultipleValues()) {
        // Remove the 'value' of the 'add more' button.
        unset($values['add_more']);

        // The original delta, before drag-and-drop reordering, is needed to
        // route errors to the correct form element.
        foreach ($values as $delta => &$value) {
          $value['_original_delta'] = $delta;
        }

        usort($values, function ($a, $b) {
          return SortArray::sortByKeyInt($a, $b, '_weight');
        });
      }

      // Let the widget massage the submitted values.
      $values = $this->massageFormValues($values, $form, $form_state);

      // Assign the values and remove the empty ones.
      $items->setValue($values);
      $items->filterEmptyItems();

      // Put delta mapping in $form_state, so that flagErrors() can use it.
      $field_state = static::getWidgetState($form['#parents'], $field_name, $form_state);
      foreach ($items as $delta => $item) {
        $field_state['original_deltas'][$delta] = isset($item->_original_delta) ? $item->_original_delta : $delta;
        unset($item->_original_delta, $item->_weight);
      }
      static::setWidgetState($form['#parents'], $field_name, $form_state, $field_state);
    }
  }

}
