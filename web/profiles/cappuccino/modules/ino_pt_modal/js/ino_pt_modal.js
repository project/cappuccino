/**
 * @file
 *Cappuccino paragraph modal JS.
 */
(function ($, Drupal, once) {

  "use strict";

  /**
   * Initialize the paragraph modal dialog.
   *
   * @param {HTMLElement} context
   *   The context element.
   * @param {object} settings
   *   The Drupal settings object.
   */
  function initParagraphModal(context, settings) {
    const $elements = $(once('paragraph-modal', '.paragraph--type--ino-pt-modal', context));
    $elements.each(function () {
      var $paragraph = $(this);
      var $button = $paragraph.find('.modal-link');
      var title = $button.text();
      var dialogId = $paragraph.attr('data-dialogId');

      $('#' + dialogId).dialog({
        title: title,
        autoOpen: false,
        draggable: false,
        modal: true,
        open: function () {
          $('body').addClass('fixed');
        },
        close: function () {
          $('body').removeClass('fixed');
        }
      });

      $('.ui-widget-overlay').click(function () {
        $('#' + dialogId).dialog('close');
      });

      // Trigger jquery-ui dialog on click on the modal link.
      $button.on('click', function () {
        $('#' + dialogId).dialog("open");
      });
    });
  }

  Drupal.behaviors.paragraphModal = {
    attach: function (context, settings) {
      initParagraphModal(context, settings);
    }
  };

})(jQuery, Drupal, once);
