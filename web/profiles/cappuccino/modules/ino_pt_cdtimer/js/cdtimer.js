(function (Drupal, drupalSettings) {
  "use strict";

  function calculateTimeDifference(targetDate, maxUnit) {
    const now = new Date();
    const diff = targetDate - now;

    if (diff <= 0) return null;

    const seconds = Math.floor(diff / 1000);
    const minutes = Math.floor(diff / (1000 * 60));
    const hours = Math.floor(diff / (1000 * 60 * 60));
    const days = Math.floor(diff / (1000 * 60 * 60 * 24));
    const weeks = Math.floor(diff / (1000 * 60 * 60 * 24 * 7));
    const months = Math.floor(diff / (1000 * 60 * 60 * 24 * 30));
    const years = Math.floor(diff / (1000 * 60 * 60 * 24 * 365));

    switch (maxUnit) {
      case 'years':
      case 'auto':
        return { total: diff, years: years, months: months % 12, weeks: weeks % 4, days: days % 7, hours: hours % 24, minutes: minutes % 60, seconds: seconds % 60 };
      case 'months':
        return { total: diff, months: months, weeks: weeks % 4, days: days % 7, hours: hours % 24, minutes: minutes % 60, seconds: seconds % 60 };
      case 'weeks':
        return { total: diff, weeks: weeks, days: days % 7, hours: hours % 24, minutes: minutes % 60, seconds: seconds % 60 };
      case 'days':
        return { total: diff, days: days, hours: hours % 24, minutes: minutes % 60, seconds: seconds % 60 };
      case 'hours':
        return { total: diff, hours: hours, minutes: minutes % 60, seconds: seconds % 60 };
      case 'minutes':
        return { total: diff, minutes: minutes, seconds: seconds % 60 };
      case 'seconds':
        return { total: diff, seconds: seconds };
      default:
        return { total: diff, years: years };
    }
  }

  function updateCountdownDisplay(event, maxUnit, units) {
    if (!event) return;

    if (maxUnit === 'years' || maxUnit === 'auto') {
      toggleVisibility(units.years, event.years);
      toggleVisibility(units.months, event.months);
      toggleVisibility(units.weeks, event.weeks);
      toggleVisibility(units.days, event.days);
      toggleVisibility(units.hours, event.hours);
      toggleVisibility(units.minutes, event.minutes);
      toggleVisibility(units.seconds, event.seconds);
    } else if (maxUnit === 'months') {
      toggleVisibility(units.months, event.months);
      toggleVisibility(units.weeks, event.weeks);
      toggleVisibility(units.days, event.days);
      toggleVisibility(units.hours, event.hours);
      toggleVisibility(units.minutes, event.minutes);
      toggleVisibility(units.seconds, event.seconds);
    } else if (maxUnit === 'weeks') {
      toggleVisibility(units.weeks, event.weeks);
      toggleVisibility(units.days, event.days);
      toggleVisibility(units.hours, event.hours);
      toggleVisibility(units.minutes, event.minutes);
      toggleVisibility(units.seconds, event.seconds);
    } else if (maxUnit === 'days') {
      toggleVisibility(units.days, event.days);
      toggleVisibility(units.hours, event.hours);
      toggleVisibility(units.minutes, event.minutes);
      toggleVisibility(units.seconds, event.seconds);
    } else if (maxUnit === 'hours') {
      toggleVisibility(units.hours, event.hours);
      toggleVisibility(units.minutes, event.minutes);
      toggleVisibility(units.seconds, event.seconds);
    } else if (maxUnit === 'minutes') {
      toggleVisibility(units.minutes, event.minutes);
      toggleVisibility(units.seconds, event.seconds);
    } else if (maxUnit === 'seconds') {
      toggleVisibility(units.seconds, event.seconds);
    }
  }

  function toggleVisibility(element, value) {
    if (element) {
      if (value > 0) {
        element.textContent = value < 10 ? `0${value}` : value;
        element.parentElement.style.display = 'block';
      } else {
        element.parentElement.style.display = 'none';
      }
    }
  }

  function startCountdown(cdDate, maxUnit, units, endTextElement) {
    const targetDate = new Date(cdDate);

    const countdownInterval = setInterval(function () {
      const event = calculateTimeDifference(targetDate, maxUnit);

      if (event) {
        updateCountdownDisplay(event, maxUnit, units);
      } else {
        clearInterval(countdownInterval);
        for (const unit in units) {
          if (units.hasOwnProperty(unit)) {
            units[unit].parentElement.style.display = 'none';
          }
        }
        endTextElement.style.display = 'block';
      }
    }, 1000);
  }

  Drupal.behaviors.CdTimerInit = {
    attach: function attach(context, settings) {
      const elements = once('cdTimerInit', '.ino-countdown[data-cdtimer-id]', context);

      elements.forEach(function (element) {
        const id = element.getAttribute('data-cdtimer-id');
        const cdSettings = drupalSettings.inoPtCdTimer[id];

        if (cdSettings) {
          const cdDate = cdSettings.date;
          const maxUnit = cdSettings.formatter === 'auto' ? 'years' : cdSettings.formatter;

          const units = {
            years: element.querySelector('div.cdtimer .yearsunit'),
            months: element.querySelector('div.cdtimer .monthsunit'),
            weeks: element.querySelector('div.cdtimer .weeksunit'),
            days: element.querySelector('div.cdtimer .daysunit'),
            hours: element.querySelector('div.cdtimer .hoursunit'),
            minutes: element.querySelector('div.cdtimer .minutesunit'),
            seconds: element.querySelector('div.cdtimer .secondsunit')
          };

          const endTextElement = element.querySelector('div.end-text');

          startCountdown(cdDate, maxUnit, units, endTextElement);
        }
      });
    }
  };

})(Drupal, drupalSettings);
