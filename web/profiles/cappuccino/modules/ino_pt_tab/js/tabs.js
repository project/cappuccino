(function ($) {
  "use strict";
  Drupal.behaviors.inoPtTabitem = {
    attach: function(context, settings) {
      const $elements = $(once('tabs-init', '.tab-container', context));
      $elements.each(function() {
        $(this).tabs();

        $(this).find('li a').hover(
          function() {
            $(this).find('.copy-link').addClass('show-link');
          },
          function() {
            $(this).find('.copy-link').removeClass('show-link');
          }
        );

        $(this).on('click', '.copy-link', function(event) {
          event.preventDefault();
          const tabId = $(this).closest('a').attr('href');
          const url = window.location.href.split('#')[0] + tabId;
          copyToClipboard(url);
        });
      });
    }
  };

  function copyToClipboard(text) {
    navigator.clipboard.writeText(text).then(() => {
      console.log('Text copied to clipboard');
    }).catch(err => {
      console.error('Error in copying text: ', err);
    });
  }
})(jQuery);
