<?php

namespace Drupal\ino_config_overrides;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystem;
use Symfony\Component\Yaml\Yaml;

/**
 * Class CustomConfigOverrides.
 *
 * @package Drupal\ino_config_overrides
 */
class CustomConfigOverrides implements ConfigFactoryOverrideInterface {

  /**
   * The configuration storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $storage;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * File prefix used for the YML files.
   */
  public const CONFIG_FILE_PREFIX = 'ino_custom_override';

  /**
   * FileSystem service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  private $fileSystem;

  /**
   * CustomConfigOverrides constructor.
   *
   * @param \Drupal\Core\Config\StorageInterface $storage
   *   The configuration storage.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The cache backend.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The file system service.
   */
  public function __construct(StorageInterface $storage, ModuleHandlerInterface $moduleHandler, CacheBackendInterface $cacheBackend, FileSystem $file_system) {
    $this->storage = $storage;
    $this->moduleHandler = $moduleHandler;
    $this->cacheBackend = $cacheBackend;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    $overrides = [];
    if ($config = $this->cacheBackend->get('CustomConfigOverrides')) {
      $overrides = $config->data;
    }
    else {
      $modules = $this->moduleHandler->getModuleList();
      foreach ($modules as $module) {
        $module_path = $module->getPath();
        // Only scan custom modules for overrides.
        if (!strstr($module_path, 'custom')) {
          continue;
        }
        if (is_dir("$module_path/config")) {
          $files = $this->fileSystem->scanDirectory("$module_path/config", '/^' . static::CONFIG_FILE_PREFIX . '.*\.yml$/');

          foreach ($files as $file) {
            $yml = Yaml::parseFile($file->uri);
            $base_config_key = $yml['base_config'];
            $base_config = $this->storage->read($base_config_key);
            unset($yml['base_config']);
            if (!empty($base_config)) {
              $new_config = $base_config;
              // Loop through the discovered yml files.
              foreach ($yml as $key => $item) {
                // The key can be a multiple-level key, such as
                // 'content.field_wysiwyg_text.weight'
                // If this is the case, we split the key components,
                // and traverse the array until the last key.
                if (strstr($key, '.')) {
                  $keys = explode('.', $key);
                  $currentValue = &$new_config;
                  $key = array_pop($keys);
                  foreach ($keys as $subKey) {
                    if (!isset($currentValue[$subKey]) && $item['action'] !== 'remove') {
                      $currentValue[$subKey] = [];
                    }
                    $currentValue = &$currentValue[$subKey];
                  }
                }
                else {
                  $currentValue = &$new_config;
                }
                switch ($item['action']) {
                  case 'set':
                    $currentValue[$key] = $item['data'];
                    break;

                  case 'append':
                    if (is_array($currentValue[$key]) && !in_array($item['data'], $currentValue[$key])) {
                      $currentValue[$key][] = $item['data'];
                    }
                    break;

                  case 'remove':
                    unset($currentValue[$key]);
                    break;

                  default:
                    break;
                }
              }
              $overrides[$base_config_key] = $new_config;
            }
          }
        }
      }
      $this->cacheBackend->set('CustomConfigOverrides', $overrides);
    }

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'CustomConfigOverrides';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
