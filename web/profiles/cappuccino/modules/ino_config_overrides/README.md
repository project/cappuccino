This module allows writing yml files to provide custom overrides to existing configuration. It is similar to Override mechanism #2 of the [Config override module](https://www.drupal.org/project/config_override) with additional features (like unsetting a config key).

Things to consider before using configuration overrides:
- Backoffice configuration forms will no longer work on overridden items. https://www.drupal.org/project/drupal/issues/2408549

Usage:
To override an existing configuration item, create a YML file in your module's 
config/overrides folder. The file should have the name 
ino_custom_override_FILENAME.yml
You can use multiple keys in a file, as demonstrated below, but only one 
action per key.
You can only use one base configuration per file.

Currently there are 3 actions implemented:

1 - Set
You can use set to replace an existing configuration item entirely.
An example YML file for, replacing the site name:
````
base_config: system.site
name:
  action: set
  data: 'New site name!'
````

2 - Append
You can use append to add an item to an array.
An example YML file, adding a new permission to the authenticated users:
````
base_config: user.role.authenticated
permissions:
  action: append
  data: 'use text format editor_embed'
````

3 - Remove
You can use remove to unset a key from an existing configuration array.
An example YML file, removing a field from the hidden fields array, and adding
it to the visible ones:
````
base_config: core.entity_form_display.paragraph.ino_pt_embed.default
hidden.field_title:
  action: remove
content.field_title:
  action: set
  data:
    weight: 0
    settings:
      size: 60
      placeholder: ''
    third_party_settings: {  }
    type: string_textfield
    region: content
````
