<?php

namespace Drupal\ino_search\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the TermSearchLink formatter.
 */
#[FieldFormatter(id: 'ino_term_link_search', label: new TranslatableMarkup('Term search link'), description: new TranslatableMarkup('Display the term name link to search page.'), field_types: ['entity_reference'])]
class TermSearchLink extends EntityReferenceLabelFormatter {

  /**
   * @var \Drupal\Core\Url Search url.
   */
  protected $search_url;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'parent' => FALSE,
      'facet' => FALSE,
      'item_limit' => '0',
      'search_key' => '',
      'search_url' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['parent'] = [
      '#title' => $this->t('Display parent name'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('parent'),
    ];

    $elements['facet'] = [
      '#title' => $this->t('Facet search'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('facet'),
    ];

    $elements['item_limit'] = [
      '#title' => $this->t('Number of items'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('item_limit'),
    ];

    $elements['search_key'] = [
      '#title' => $this->t('Search key name'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('search_key'),
    ];

    $elements['search_url'] = [
      '#title' => $this->t('Search url path'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('search_url'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->getSetting('parent') ? $this->t('Show parent') : $this->t('Parent not show');
    return $summary;
  }

  /**
   * Generate options for the link.
   *
   * @param string $key
   *   key used in the view.
   * @param string $value
   *   value used in the view.
   *
   * @param array $options
   *   Options array.
   */
  public function generateOptions($key, $value) {
    $is_facet = $this->getSetting('facet');

    $options = [];

    if ($is_facet) {
      $options = [
        'query' => [
          'f' => [
            $key . ':' . $value,
          ],
        ],
      ];
    }
    else {
      $options = [
        'query' => [
          $key => $value,
        ],
      ];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $output_as_link = $this->getSetting('link');
    $show_parent = $this->getSetting('parent');
    $search_url = $this->getSetting('search_url');

    $this->search_url = Url::fromUri("internal:" . $search_url);

    $count = 0;
    $item_limit = $this->getSetting('item_limit');

    /** @var \Drupal\taxonomy\Entity\Term $entity */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {

      if (!empty($item_limit) && is_numeric($item_limit) && $count >= $item_limit) {
        break;
      }
      else {
        $count++;
      }
      $element = [];
      $label = $entity->label();

      if ($output_as_link && !$entity->isNew()) {
        if ($show_parent) {
          $parent_tid = $entity->parent->target_id;
          if ($parent_tid) {
            $parent = $entity->parent->entity;
            $this->getParent($parent, $element);
            $elements[$delta] = $element;
          }
        }

        $search_key = $this->getSetting('search_key');

        $options = $this->generateOptions($search_key, $entity->id());

        // Check if the entity has the "field_t_style" field and it's not empty.
        if ($entity->hasField('field_t_style') && !$entity->field_t_style->isEmpty()) {
          // Get the first value of the "field_t_style" field.
          $style_class = $entity->field_t_style->getValue()[0]['value'];
          // Add the class to the #options array.
          $options['attributes']['class'][] = $style_class;
        }

        $elements[$delta][] = [
          '#type' => 'link',
          '#title' => $label,
          '#url' => clone $this->search_url,
          '#options' => $options,
        ];

        if (!empty($items[$delta]->_attributes)) {
          $elements[$delta]['#options'] += ['attributes' => []];
          $elements[$delta]['#options']['attributes'] += $items[$delta]->_attributes;
          // Unset field item attributes since they have been included in the
          // formatter output and shouldn't be rendered in the field template.
          unset($items[$delta]->_attributes);
        }
      }
      else {
        $elements[$delta] = ['#plain_text' => $label];
      }
      $elements[$delta]['#cache']['tags'] = $entity->getCacheTags();
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity) {
    return $entity->access('view label', NULL, TRUE);
  }

  /**
   * Generated parent render array.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   *
   * @param array $element
   *   Render array.
   */
  private function getParent(EntityInterface $entity, array &$element) {
    $output_as_link = $this->getSetting('link');
    $is_facet = $this->getSetting('facet');

    $parent_tid = $entity->parent->target_id;
    if ($parent_tid) {
      $parent = $entity->parent->entity;
      $element = $this->getParent($parent, $element);
    }

    $search_key = $this->getSetting('search_key');

    if ($output_as_link) {

      $options = $this->generateOptions($search_key, $entity->id());

      $element[] = [
        '#type' => 'link',
        '#title' => $entity->label(),
        '#url' => clone $this->search_url,
        '#options' => $options,
      ];
    }
    else {
      $element[] = ['#plain_text' => $entity->label()];
    }

    $element[] = [
      '#markup' => '<span class="separator"> > </span>',
    ];
  }

}
