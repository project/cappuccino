/**
 * @file
 * Sort functionality with links.
 */
(function ($) {
  var handler;
  Drupal.behaviors.searchPageSort = {
    attach: function (context, settings) {
      handler = this;
      var url = window.location.href;
      var sortType = this.urlParam('sort_by', url);
      if (sortType === 'created') {
        $('.sort-links a[data-sort="created"]').addClass('is-active');
      }
      else {
        $('.sort-links a[data-sort="search_api_relevance"]').addClass('is-active');
      }

      const $elements = $(once('sortLinks', '.sort-links a', context));
      $elements.each(function () {
        $(this).on('click', function () {
          var newSort = $(this).data('sort');
          url = handler.replaceParam('sort_by', url, newSort);
          window.location.href = url;
        });
      });

    },
    urlParam: function ( name, url ) {
      if (!url) url = location.href;
      name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
      var regexS = "[\\?&]" + name + "=([^&#]*)";
      var regex = new RegExp( regexS );
      var results = regex.exec( url );
      return results == null ? null : results[1];
    },
    replaceParam: function (key, sourceUrl, newParam) {
      var rtn = sourceUrl.split("?")[0],
          param,
          params_arr = [],
          hasParam = false,
          queryString = (sourceUrl.indexOf("?") !== -1) ? sourceUrl.split("?")[1] : "";
      if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
          param = params_arr[i].split("=")[0];
          if (param === key) {
            params_arr.splice(i, 1, key + '=' + newParam);
            hasParam = true;
          }
        }
        if (!hasParam) {
          params_arr.push(key + '=' + newParam);
        }
        rtn = rtn + "?" + params_arr.join("&");
      }
      else {
        rtn = rtn + "?" + key + '=' + newParam;
      }
      return rtn;
    },
  }
})(jQuery);
