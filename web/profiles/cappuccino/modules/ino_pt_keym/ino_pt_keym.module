<?php

/**
 * @file
 * Contains hook implementations.
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Implements hook_preprocess_HOOK().
 *
 * {@inheritdoc}
 */
function ino_pt_keym_preprocess_paragraph__ino_pt_keym(&$variables) {
  /** @var Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  if ($paragraph->hasField('field_ph_keym_layout') && !$paragraph->get('field_ph_keym_layout')->isEmpty()) {
    // Some layouts can have specific region names.
    $layout_id = $paragraph->get('field_ph_keym_layout')->value;
  }
  else {
    // Layout field is not defined set first layout to default.
    $options = [
      'restrict' => [
        'ino_pt_keym',
      ],
    ];

    $layouts = ino_content_editing_layout_allowed_values($paragraph->getFieldDefinition('field_ph_keym_unit')->getFieldStorageDefinition(), $paragraph, $options);
    if (!empty($layouts)) {
      $layout_ids = array_keys($layouts);
      $layout_id = array_shift($layout_ids);
    }
  }

  if (!empty($layout_id)) {
    $original = $variables['content'];
    // Getting an eventually changed title from content instead of
    // $paragraph->get('field_title')->value.
    $paragraph_title = isset($variables['content']['field_title'][0])
      ? $variables['content']['field_title']
      : NULL;

    $formatted_amount = '';
    if (isset($variables['content']['field_ph_keym_unit'])) {
      $unit_position_value = $paragraph->get('field_ph_keym_unit_position')->value;
      $unit = $paragraph->get('field_ph_keym_unit')->value;
      $formatted_amount = rtrim($paragraph->get('field_ph_keym_amount')->value, '0');
      if (!empty($unit_position_value)) {
        if ('Suffix' == $unit_position_value) {
          $formatted_amount = '<span class="ino-pt-keym-amount">' . $formatted_amount . '</span>' . '<span class="ino-pt-keym-unit">' . $unit . '</span>';
        }
        else {
          $formatted_amount = '<span class="ino-pt-keym-unit">' . $unit . '</span> ' . '<span class="ino-pt-keym-amount">' . $formatted_amount . '</span>';
        }
      }
    }
    $variables['content']['field_ph_keym_amount'][0]['#markup'] = $formatted_amount;
    $variables['content']['field_ph_keym_amount']['#attributes']['data-delimiter'] = $paragraph->get('field_ph_keym_delimiter')->value;

    $content = [
      'amount' => $variables['content']['field_ph_keym_amount'],
      'icon' => $variables['content']['field_ph_keym_icon'],
      'short_desc' => $variables['content']['field_ph_keym_short_desc'],
    ];
    $regions = [
      'top' => $paragraph_title,
      'item' => $content,
    ];

    /** @var Drupal\Core\Layout\LayoutPluginManager $layout_plugin_manager */
    $layout_plugin_manager = \Drupal::service('plugin.manager.core.layout');
    /** @var Drupal\Core\Layout\LayoutDefault $layout_instance */
    $layout_instance = $layout_plugin_manager->createInstance($layout_id);
    $variables['content'] = $layout_instance->build($regions);
    $variables['content']['original'] = $original;
    $variables['content']['#attached']['library'][] = 'ino_pt_keym/keym';
  }

  if (!$paragraph->get('field_ph_keym_style')->isEmpty()) {
    // Add style.
    $variables += ['attributes' => []];
    $variables['attributes'] += ['class' => []];
    $variables['attributes']['class'] = array_merge($variables['attributes']['class'], explode(', ', $paragraph->get('field_ph_keym_style')->getString()));
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 *
 * Widget type : Paragraphs EXPERIMENTAL : paragraphs.
 *
 * Not called on Paragraphs Edit forms.
 */
function ino_pt_keym_field_widget_paragraphs_form_alter(&$element, FormStateInterface $form_state, $context) {
  if (isset($element['#paragraph_type'], $element['subform']['#parents']) && 'ino_pt_keym' == $element['#paragraph_type']) {
    _ino_pt_keym_subform_alter($element['subform']);
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Paragraphs Edit form.
 *
 * {@inheritdoc}
 */
function ino_pt_keym_form_paragraph_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  /** @var Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $form_state->getFormObject()->getEntity();
  if ('ino_pt_keym' == $paragraph->getType()) {
    _ino_pt_keym_subform_alter($form);
  }
}

/**
 * Key metrics PT (sub)form alter.
 */
function _ino_pt_keym_subform_alter(array &$subform) {
  if (!isset($subform['field_ph_keym_layout']) || empty($subform['field_ph_keym_layout'])) {
    return;
  }
  $keym_layout_full_name = ino_content_editing_form_element_states_path($subform, 'field_ph_keym_layout');
  $subform['field_ph_keym_unit']['widget']['0']['value']['#size'] = 10;
  $subform['unit'] = [
    '#type' => 'container',
    '#attributes' => ['class' => ['container-inline']],
    '#weight' => 2,
    'keym_unit' => $subform['field_ph_keym_unit'],
    'keym_unit_position' => $subform['field_ph_keym_unit_position'],
  ];
  unset($subform['field_ph_keym_unit'], $subform['field_ph_keym_unit_position']);
}

/**
 * Set dynamic allowed values for the layout field.
 *
 * @param \Drupal\field\Entity\FieldStorageConfig $definition
 *   The field definition.
 * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
 *   The entity being created if applicable.
 *
 * @return array
 *   An array of possible key and value options.
 */
function ino_pt_keym_layout_allowed_values(FieldStorageConfig $definition, ContentEntityInterface $entity = NULL) {
  $options = [
    'restrict' => [
      'ino_pt_keym',
    ],
  ];

  return ino_content_editing_layout_allowed_values($definition, $entity, $options);
}

/**
 * Loads the styles from the custom configuration file.
 *
 * @return array
 *   The configuration array.
 */
function _ino_pt_keym_get_styles() {
  if (\Drupal::hasService('ino_pt_helper.theme_configuration_provider')) {
    return \Drupal::service('ino_pt_helper.theme_configuration_provider')->getStyleOptions('ino_pt_keym');
  }
  return [];
}
