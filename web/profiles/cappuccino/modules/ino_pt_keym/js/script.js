/**
 * @file
 * Key metrics animations.
 */
(function() {

  function isInViewport(element) {
    const rect = element.getBoundingClientRect();
    const viewportHeight = window.innerHeight || document.documentElement.clientHeight;
    const viewportTop = 0;
    const viewportBottom = viewportTop + viewportHeight;

    return rect.bottom > viewportTop && rect.top < viewportBottom;
  }

  function animateNumber(element, fromValue, toValue, duration, rounding, delimiter) {
    const startTime = performance.now();

    function step(currentTime) {
      const elapsed = currentTime - startTime;
      const progress = Math.min(elapsed / duration, 1);

      const currentValue = fromValue + (toValue - fromValue) * progress;
      const formattedValue = currentValue.toFixed(rounding).replace(/\B(?=(\d{3})+(?!\d))/g, delimiter);

      element.textContent = formattedValue;

      if (progress < 1) {
        requestAnimationFrame(step);
      }
    }

    requestAnimationFrame(step);
  }

  function KeyMetricsAnimate() {
    const elements = document.querySelectorAll('.layout--ino-pt-keym-basic .ino-pt-keym-amount');
    elements.forEach(function(element) {
      const number = element.textContent.replace(/,/g, '').replace(",", ".");
      const delimiter = element.parentElement.dataset.delimiter || '';
      const decimalNumber = countDecimals(number);
      const toValue = parseFloat(number);
      const duration = 1400;
      const fromValue = 0;

      if (isInViewport(element)) {
        element.classList.add('ino-pt-keym-amount-in-viewport');
      }
      if (
        element.classList.contains('ino-pt-keym-amount-in-viewport') &&
        !element.classList.contains('stopanimation')
      ){
        animateNumber(element, fromValue, toValue, duration, decimalNumber, delimiter);
        element.classList.add('stopanimation');
      }
    });
  }

  function countDecimals(value) {
    if (Math.floor(value) === Number(value)) return 0;
    return value.split(".")[1]?.length || 0;
  }

  document.addEventListener("DOMContentLoaded", function() {
    KeyMetricsAnimate();
    document.addEventListener("touchmove", KeyMetricsAnimate, false);
    document.addEventListener("scroll", KeyMetricsAnimate, false);
  });

})();
