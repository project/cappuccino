<?php

namespace Drupal\ino_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\media\Plugin\Field\FieldFormatter\OEmbedFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the https 'oembed' formatter.
 */
#[FieldFormatter(id: 'https_oembed_formatter', label: new TranslatableMarkup('Https OEmbed Formatter'), field_types: ['string'])]
class HttpsOEmbedFormatter extends OEmbedFormatter {

  /*
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    foreach ($elements as &$element) {
      if (isset($element['#attributes']['src'])) {
        $element['#attributes']['src'] = str_replace('http://', 'https://', $element['#attributes']['src']);
      }
    }
    return $elements;
  }
}
