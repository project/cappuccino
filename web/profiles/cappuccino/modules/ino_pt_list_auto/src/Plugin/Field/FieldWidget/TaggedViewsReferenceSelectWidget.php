<?php
namespace Drupal\ino_pt_list_auto\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\viewsreference\Plugin\Field\FieldWidget\ViewsReferenceSelectWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\ino_pt_list_auto\Decorator\BaseArgumentHandler;
use Drupal\ino_pt_list_auto\Decorator\TermDecorator;
use Drupal\ino_pt_list_auto\Decorator\ContentTypeDecorator;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'tagged_viewsreference_select' widget.
 */
#[FieldWidget(id: 'tagged_viewsreference_select', label: new TranslatableMarkup('Tagged Views reference select'), field_types: ['viewsreference'])]
class TaggedViewsReferenceSelectWidget extends ViewsReferenceSelectWidget {

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The decorated argument handler.
   *
   * @var \Drupal\ino_pt_list_auto\Decorator\ArgumentHandlerInterface
   */
  protected $argumentHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $select_element = parent::formElement($items, $delta, $element, $form, $form_state);

    $altered_views = ['tagged_content'];

    $view_name = $select_element['target_id']['#default_value'] ?? '';

    if (empty($view_name) || !in_array($view_name, $altered_views)) {
      return $select_element;
    }

    // Hide the arguments field.
    $select_element['options']['argument']['#access'] = FALSE;

    $select_element['options']['#attributes'] = ['class' => ['tagged-selects']];

    $this->argumentHandler = new BaseArgumentHandler();

    switch ($view_name) {
      case 'tagged_content':
        $this->argumentHandler = new TermDecorator($this->argumentHandler, $this->entityTypeManager, ['tags']);
        $this->argumentHandler = new ContentTypeDecorator($this->argumentHandler);

        $terms = $select_element['options']['argument']['#default_value'];
        $content_type = '';

        if (strpos($select_element['options']['argument']['#default_value'], '/') !== FALSE) {
          list($terms, $content_type) = explode('/', $select_element['options']['argument']['#default_value']);
        }

        $arguments = [
          'terms' => $terms,
          'content_type' => $content_type,
        ];
        break;
    }

    $this->argumentHandler->formBuild($items, $delta, $select_element, $form, $form_state, $arguments);

    // Attach custom validation to handle the field processing.
    $select_element['#element_validate'][] = [$this, 'customElementValidate'];

    return $select_element;
  }

  /**
   * Custom validation for processing the taxonomy selections.
   */
  public function customElementValidate($element, FormStateInterface $form_state) {
    if (!empty($this->argumentHandler)) {
      $arguments = $this->argumentHandler->validate($form_state);
      $form_state->setValueForElement($element['options']['argument'], $arguments);
    }
  }
}
