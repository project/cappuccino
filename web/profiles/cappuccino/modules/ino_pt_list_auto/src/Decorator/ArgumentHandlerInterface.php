<?php
namespace Drupal\ino_pt_list_auto\Decorator;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an interface for argument handlers.
 */
interface ArgumentHandlerInterface {

  /**
   * Builds the form elements for this argument handler.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field item list.
   * @param int $delta
   *   The delta value.
   * @param array $element
   *   The form element.
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $arguments
   *   The arguments array.
   */
  public function formBuild(FieldItemListInterface $items, $delta, array &$element, array &$form, FormStateInterface $form_state, array $arguments);

  /**
   * Validates the form elements for this argument handler.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validate(FormStateInterface $form_state);
}
