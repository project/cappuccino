<?php
namespace Drupal\ino_pt_list_auto\Decorator;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Provides a term decorator for the argument handler.
 */
class TermDecorator implements ArgumentHandlerInterface {
  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The decorated argument handler.
   *
   * @var \Drupal\ino_pt_list_auto\ArgumentHandlerInterface
   */
  protected $argumentHandler;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Vocabularies to use.
   *
   * @var array
   */
  protected $vocabularies;

  /**
   * Constructs a new ContentTypeDecorator object.
   *
   * @param \Drupal\ino_pt_list_auto\ArgumentHandlerInterface $argumentHandler
   *   The decorated argument handler.
   */
  public function __construct(ArgumentHandlerInterface $argumentHandler, EntityTypeManagerInterface $entityTypeManager, array $vocabularies = []) {
    $this->argumentHandler = $argumentHandler;
    $this->entityTypeManager = $entityTypeManager;
    $this->vocabularies = $vocabularies;
  }

  /**
   * Get taxonomy terms for a given vocabulary.
   */
  private function getTaxonomyTerms($vocab_name) {
    $options = [];
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vocab_name);
    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function formBuild(FieldItemListInterface $items, $delta, array &$element, array &$form, FormStateInterface $form_state, array $arguments) {
    $this->argumentHandler->formBuild($items, $delta, $element, $form, $form_state, $arguments);
    foreach ($this->vocabularies as $vocab) {
      $options = $this->getTaxonomyTerms($vocab);
      if (!empty($options)) {
        // Extract term IDs from the arguments value.
        $term_ids = explode(',', str_replace('+', ',', $arguments['terms']));

        // Filter term_ids for the specific vocabulary.
        $default_values = [];
        foreach ($term_ids as $term_id) {
          if (isset($options[$term_id])) {
            $default_values[] = $term_id;
          }
        }

        $element['options']['taxonomy_terms_' . $vocab] = [
          '#type' => 'select',
          '#title' => ucfirst($vocab),
          '#options' => $options,
          '#multiple' => TRUE,
          '#default_value' => $default_values,
        ];
      }
    }

    $default_operator = (strpos($arguments['terms'], '+') !== FALSE) ? 'OR' : 'AND';
    $element['options']['operator'] = [
      '#type' => 'radios',
      '#title' => $this->t('Relationship'),
      '#default_value' => $default_operator,
      '#options' => [
        'AND' => $this->t('AND'),
        'OR' => $this->t('OR'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validate(FormStateInterface $form_state) {
    $arguments = $this->argumentHandler->validate($form_state);
    $values = [];
    $input = $form_state->getValue('field_ph_lista_list');
    foreach ($this->vocabularies as $vocab) {
      if (!empty($input[0]['options']['taxonomy_terms_' . $vocab])) {
        $values = array_merge($values, $input[0]['options']['taxonomy_terms_' . $vocab]);
      }
    }
    $args = 'all';
    if (!empty($values)) {
      $operator = $input[0]['options']['operator'] == 'OR' ? '+' : ',';
      $args = implode($operator, $values);
    }

    if (!empty($arguments)) {
      $arguments .= '/';
    }
    return $arguments . $args;
  }
}
