<?php
namespace Drupal\ino_pt_list_auto\Decorator;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base class for argument handlers.
 */
class BaseArgumentHandler implements ArgumentHandlerInterface {

  /**
   * {@inheritdoc}
   */
  public function formBuild(FieldItemListInterface $items, $delta, array &$element, array &$form, FormStateInterface $form_state, array $arguments) {
  }

  /**
   * {@inheritdoc}
   */
  public function validate(FormStateInterface $form_state) {
    return '';
  }
}
