<?php
namespace Drupal\ino_pt_list_auto\Decorator;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides a content type decorator for the argument handler.
 */
class ContentTypeDecorator implements ArgumentHandlerInterface {
  use StringTranslationTrait;

  /**
   * The decorated argument handler.
   *
   * @var \Drupal\ino_pt_list_auto\ArgumentHandlerInterface
   */
  protected $argumentHandler;

  /**
   * Constructs a new ContentTypeDecorator object.
   *
   * @param \Drupal\ino_pt_list_auto\ArgumentHandlerInterface $argumentHandler
   *   The decorated argument handler.
   */
  public function __construct(ArgumentHandlerInterface $argumentHandler) {
    $this->argumentHandler = $argumentHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function formBuild(FieldItemListInterface $items, $delta, array &$element, array &$form, FormStateInterface $form_state, array $arguments) {
    $this->argumentHandler->formBuild($items, $delta, $element, $form, $form_state, $arguments);

    $content_types = ['ino_news' => 'News', 'event' => 'Event'];
    $content_type_array = explode(',', $arguments['content_type']);
    $element['options']['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content Type'),
      '#options' => $content_types,
      '#default_value' => $content_type_array,
      '#multiple' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validate(FormStateInterface $form_state) {
    $arguments = $this->argumentHandler->validate($form_state);
    $input = $form_state->getValue('field_ph_lista_list');
    $args = 'all';
    if (!empty($input[0]['options']['content_type'])) {
      $args = implode(',', $input[0]['options']['content_type']);
    }
    if (!empty($arguments)) {
      $arguments .= '/';
    }
    return $arguments . $args;
  }
}
