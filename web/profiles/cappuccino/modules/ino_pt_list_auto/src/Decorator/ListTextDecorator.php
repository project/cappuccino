<?php
namespace Drupal\ino_pt_list_auto\Decorator;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Provides a list text decorator for the argument handler.
 */
class ListTextDecorator implements ArgumentHandlerInterface {
  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The decorated argument handler.
   *
   * @var \Drupal\ino_pt_list_auto\ArgumentHandlerInterface
   */
  protected $argumentHandler;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field name.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * Constructs a new ContentTypeDecorator object.
   *
   * @param \Drupal\ino_pt_list_auto\ArgumentHandlerInterface $argumentHandler
   *   The decorated argument handler.
   */
  public function __construct(ArgumentHandlerInterface $argumentHandler, EntityTypeManagerInterface $entityTypeManager, string $fieldName) {
    $this->argumentHandler = $argumentHandler;
    $this->entityTypeManager = $entityTypeManager;
    $this->fieldName = $fieldName;
  }

  /**
   * {@inheritdoc}
   */
  public function formBuild(FieldItemListInterface $items, $delta, array &$element, array &$form, FormStateInterface $form_state, array $arguments) {
    $this->argumentHandler->formBuild($items, $delta, $element, $form, $form_state, $arguments);

    // Load the field and check if it's of type 'list_string'.
    $field = $this->entityTypeManager->getStorage('field_storage_config')->load('node.' . $this->fieldName);
    if ($field && $field->getType() == 'list_string') {
      $allowed_values = $field->getSetting('allowed_values');
      $default_value = explode(',', $arguments['list']);
      $field_name_parts = explode('_', $this->fieldName);
      // Remove 'field' part.
      array_shift($field_name_parts);
      $processed_field_label = ucwords(implode(' ', $field_name_parts));
      $element['options'][$this->fieldName] = [
        '#type' => 'select',
        '#title' => $this->t($processed_field_label),
        '#options' => $allowed_values,
        '#default_value' => $default_value,
        '#multiple' => TRUE,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate(FormStateInterface $form_state) {
    $arguments = $this->argumentHandler->validate($form_state);
    $input = $form_state->getValue('field_ph_lista_list');
    $args = 'all';
    if (!empty($input[0]['options'][$this->fieldName])) {
      $args = implode(',', $input[0]['options'][$this->fieldName]);
    }
    if (!empty($arguments)) {
      $arguments .= '/';
    }
    return $arguments . $args;
  }
}
