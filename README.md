Cappuccino
====

This is a Drupal 11 distribution which serves as a base for other projects.


Installing
--------------------------------

1. Clone the repository, or download the latest version.

2. Run composer install, to download all the dependencies.

3. Point your webhost to the web folder.

4. Build the theme (ino_basetheme) with npm and gulp.
Inside the theme folder run:
npm install

/* Compile on the fly the theme for development + launches browser-sync */
npx gulp watch

/* Compile the theme for development mode */
npx gulp sass-dev

/* Compile the theme for production */
npx gulp sass-prod

5. Make sure that the "config_sync_directory" configuration points to the config/sync
folder inside the "cappuccino" profile.

6. Install Drupal with the "drush site:install --existing-config" command, as currently
installing it through the UI is broken. The Cappuccino profile will be selected by default.
The initial configuration will be installed as well.
